/*************************
*    Cosmo Titan         *
* Game core definitions  *
* Author: Oleg Getmansky *
*************************/

Game.Ingame = function() {

};

Game.Ingame.prototype = {
   preload : preload,
   create : create,
   update : update,
   render : render
};

var inputMenu = false;
var inputCredits = false;
var ingame = false;
var firstLaunch = true;

var rightKey;
var leftKey; 
var Akey; 
var Dkey; 
var fps;
var buttonMute;
var hp;
var cash;
var enemiesCount;
var levelNumber;
var lastCash = 0;

var audioMuted = false;
if(localStorage.getItem('audioMuted') == 'yes')
   audioMuted = true;

var audioBackgroundGame;
var audioBackgroundMenu;
var audioButtonClick;
var audioHeroShot;
var audioEnemyShotGreen;
var audioEnemyShotOrange;
var audioEnemyExplode;
var audioEnemyHurt;
var audioHeroLaserShot;
var audioHeroShield;
var audioHeroMissle;
var audios = [];

String.prototype.insertAt = function(index, string) { 
   return this.substr(0, index) + string + this.substr(index);
}

String.prototype.replaceAt=function(index, character) {
   return this.substr(0, index) + character + this.substr(index + character.length);
}

var game = new Phaser.Game(600, 870, Phaser.AUTO, 'cosmotitan');

game.state.add('Boot', Game.Boot);
game.state.add('Preloader', Game.Preloader);
game.state.add('Ingame', Game.Ingame);

game.state.start('Boot');

var random = new Phaser.RandomDataGenerator([new Date()])

var Context = {
   showLogo : false,

   levels : [level1, level2, level3, level4, level5, level6, level7, level8, level9, level10, level11, level12, level13, level14, level15],
   enemies : [enemySoldier, enemyArmadillo, enemyFighter, enemyHeavyArmadillo, enemyTurret1, enemyTurret2, enemyNeutralizer],
   bullets : [dollarBullet, bullet1, bullet2, neutralizerBullet, heroBullet1, heroBullet2, heroBullet3, homingMissle],
   allUpgrades : [laserUpgrade, homingMissleUpgrade, immortalShield, extraBullets, noUpgrade, bulletUpgrade1, bulletUpgrade2, bulletUpgrade3, healthUpgrade, attackSpeedUpgrade],

   primaryUpgrades : [],
   secondaryUpgrades : [extraBullets, homingMissleUpgrade, immortalShield, laserUpgrade],
   bulletUpgrades : [bulletUpgrade1, bulletUpgrade2, bulletUpgrade3],
   statUpgrades : [healthUpgrade, attackSpeedUpgrade],

   playerSpeed : 350,

   playerFireDelay : 500,

   playerBulletType : heroBullet2,
   playerMaxHp : 20,
   playerHp : 20,
   playerCash : 0,

   playerHpBarStartX : 18,
   playerHpBarStartY : 842,
   playerHpIconWidth : 20,
   playerHpCellWidth : 9,

   group_logoScreen : null,
   group_menuScreen : null,
   group_playScreen : null,
   group_upgradeScreen : null,
   group_creditScreen : null,
   group_creditNames : null,
   upgradeTweens : [],

   group_bullets : null,
   group_units : null,
   group_hud : null,
   group_shield : null,
   gradeDots : [],

   init : function() {
      this.group_logoScreen = game.add.group();
      this.group_logoScreen.visible = false;

      this.group_menuScreen = game.add.group();
      this.group_menuScreen.visible = false;

      this.group_playScreen = game.add.group();
      this.group_playScreen.visible = false;

      this.group_upgradeScreen = game.add.group();
      this.group_upgradeScreen.visible = false;

      this.group_creditScreen = game.add.group();
      this.group_creditScreen.visible = false;

      this.group_creditNames = game.add.group();
      this.group_menuButtons = game.add.group();

      this.group_mainmenuTweens = game.add.group();

      this.buildMenu();
      this.buildCreditScreen();

      restoreProgress();

      Context.leftUpgradeIcon = Context.equippedBulletUpgrade.icon;
      Context.rightUpgradeIcon = Context.equippedUpgrade.icon;

      Context.graphics.leftSprite = game.add.sprite(212+0, 237+0, Context.leftUpgradeIcon);
      //Context.graphics.leftSprite.anchor.setTo(0, 0.5);
      Context.graphics.rightSprite = game.add.sprite(346+0, 237+0, Context.rightUpgradeIcon);
      //Context.graphics.rightSprite.anchor.setTo(0.5, 0.5);
      
      this.buildUpgradeScreen();

      audios.push(audioBackgroundGame = game.add.audio('background_game', 1, true));
      audioBackgroundGame.override = true;
      audios.push(audioBackgroundMenu = game.add.audio('background_menu', 1, true));
      audioBackgroundMenu.override = true;
      audios.push(audioButtonClick = game.add.audio('button_click'));
      audios.push(audioHeroShot = game.add.audio('hero_shot'));
      audios.push(audioEnemyShotGreen = game.add.audio('enemy_shot_green'));
      audios.push(audioEnemyShotOrange = game.add.audio('enemy_shot_orange'));
      audios.push(audioEnemyExplode = game.add.audio('enemy_explode'));
      audios.push(audioEnemyHurt = game.add.audio('enemy_hurt'));
      audios.push(audioHeroLaserShot = game.add.audio('hero_laser_shot'));
      audios.push(audioHeroShield = game.add.audio('hero_shield'));
      audios.push(audioHeroMissle = game.add.audio('hero_missle'));
   },

   buildMenu : function() {
      var _screen = this.group_menuScreen;
      var _tweens = this.group_mainmenuTweens;

      _screen.add(game.add.sprite(0, 0, 'background_MainMenu'));
      _screen.add(game.add.sprite(0, 0, 'background_border'));
      _screen.add(_tweens);
      _tweens.add(game.add.sprite(90, 100, 'mainmenu_e1'));
      _tweens.add(game.add.sprite(138, 220, 'mainmenu_e2'));

      var _buttons = Context.group_menuButtons;
      _screen.add(_buttons);

      var button_start = game.add.button(305, 350, 'button_MainMenu', function() {
         if(!inputMenu) return;
         audioButtonClick.play();
         startGame();
      }, Context, 1, 0, 1, 0);
      button_start.anchor.set(0.5);
      _buttons.add(button_start);
      _buttons.add(game.add.bitmapText(275, 332, 'impact','START', 30));

      var resetText;
      var button_reset = game.add.button(305, 400+50, 'button_MainMenu', function() {
         if(!inputMenu) return;
         resetProgress();
         resetText.tint = 0x010101;
      }, Context, 1, 0, 1, 0);
      button_reset.anchor.set(0.5);
      _buttons.add(button_reset);
      resetText = game.add.bitmapText(235, 385+50, 'impact','RESET PROGRESS', 26)
      _buttons.add(resetText);

      var button_credits = game.add.button(305, 450+100, 'button_MainMenu', function() {
         if(!inputMenu) return;
         audioButtonClick.play();
         hideMenuScreen(showCreditScreen);
      }, Context, 1, 0, 1, 0);
      button_credits.anchor.set(0.5);
      _buttons.add(button_credits);
      _buttons.add(game.add.bitmapText(265, 432+100, 'impact','CREDITS', 30));

      /*var button_start = game.add.button(game.world.centerX, game.world.centerY+100, 'button_start', function() {
         game.scale.startFullScreen();
      }, Context, 1, 0, 1);
      button_start.anchor.set(0.5);
      this.group_menuScreen.add(button_start);*/
   },

   buildCreditScreen : function() {
      var _screen = this.group_creditScreen;
      var _names = this.group_creditNames;
      var _buttons = game.add.group();

      _screen.add(game.add.sprite(0, 0, 'background_MainMenu'));
      _screen.add(game.add.sprite(0, 0, 'background_border'));
      _screen.add(_buttons);

      var button_back = game.add.button(309, 644, 'big_button', function() {
         if(!inputCredits) return;
         audioButtonClick.play();
         hideCreditScreen(showMenuScreen);
      }, Context, 1, 0, 1, 0);
      button_back.anchor.set(0.5);
      _names.add(button_back);
      _names.add(game.add.bitmapText(280, 625, 'impact','BACK', 30));

      _names.add(game.add.sprite(90, 200, 'credits_bg'));
      _screen.add(_names);

      _names.add(game.add.bitmapText(125, 232, 'ocra','Oleg Getmansky', 26));
      _names.add(game.add.bitmapText(235, 252, 'ocra','aka OlegusGetman', 26));
      _names.add(game.add.bitmapText(185, 280, 'ocra','olegusgetman.ru', 26));
      _names.add(game.add.bitmapText(125, 442, 'ocra','Daniil Naletov', 26));
      _names.add(game.add.bitmapText(335, 462, 'ocra','aka condor', 26));
      _names.add(game.add.bitmapText(125, 485, 'ocra','daniilnaletov@gmail.com', 26));

      /*var button_start = game.add.button(game.world.centerX, game.world.centerY+100, 'button_start', function() {
         game.scale.startFullScreen();
      }, Context, 1, 0, 1);
      button_start.anchor.set(0.5);
      this.group_menuScreen.add(button_start);*/
   },

   buildUpgradeScreen : function() {
      clearPlayScreen();
      Context.group_playScreen.visible = false;

      var _screen = Context.group_upgradeScreen;
      var _back = game.add.group();
      var _buttons = game.add.group();
      var _tweens = Context.upgradeTweens;
      _buttons.inputEnabled = true;
      var _gui = game.add.group();
      _gui.inputEnabled = true;
      var _upgradePanel = game.add.group();
      _upgradePanel.inputEnabled = false;
      _upgradePanel.visible = false;
      var _dots = game.add.group();

      _screen.add(_back);
      _tweens.push(_screen.add(_buttons), true)
      _tweens.push(_screen.add(_gui), true);
      _tweens.push(_screen.add(_dots), true);
      _tweens.push(_screen.add(_upgradePanel), true);

      _back.add(game.add.sprite(0, 0, 'background_MainMenu'));
      _back.add(game.add.sprite(0, 0, 'background_border'));
      _gui.add(game.add.sprite(80, 30, 'grade_bg'));
      var menuTitan = game.add.sprite(295, 180, 'menu_titan');
      Context.titanImage = menuTitan;
      game.physics.enable(menuTitan, Phaser.Physics.ARCADE);
      menuTitan.anchor.setTo(0.5, 0.5);
      menuTitan.body.angularVelocity = 10;

      _gui.add(menuTitan);
      _gui.add(Context.graphics.leftSprite);
      _gui.add(Context.graphics.rightSprite);

      var cash = game.add.bitmapText(255, 60, 'impact',Context.playerCash.toString(), 22);
      Context.cashText = cash;
      _gui.add(cash);
      var hp = game.add.bitmapText(335, 60, 'impact',Context.playerMaxHp.toString(), 22);
      _gui.add(hp);

      function addUpgrade(buttonSprite, upgrade, x, y, canEquip, equipCallback, gradeCallback) {
         var button = game.add.button(x, y, buttonSprite, function() {
            if(!_gui.inputEnabled  || alreadyStarted) return;
            audioButtonClick.play();
            showDescription(upgrade, x, y, canEquip, equipCallback, gradeCallback, true);
         }, null, 1, 0, 1, 0);

         var icon = game.add.sprite(x+28, y+28, upgrade.icon);
         icon.anchor.setTo(0.5, 0.5);

         _gui.add(button);
         _gui.add(icon);

         for(var i = 1; i <= upgrade.level; i++) {
            var dot = game.add.sprite(x + 50 + 23*(i-1), y + 19, 'grade_dot');
            Context.gradeDots.push();
            _dots.add(dot);
         }
      }

      function showDescription(upgrade, x, y, canEquip, equipCallback, gradeCallback, doFade) {
         _gui.inputEnabled = false;
         _buttons.inputEnabled = false;
         _upgradePanel.inputEnabled = true;
         _upgradePanel.visible = true;

         var upgradePanelBg = game.add.sprite(0,0, 'upgrade_panel_bg');
         upgradePanelBg.inputEnabled = true;
         upgradePanelBg.events.onInputOver.add(function() {
            return true;
         }, this);
         _upgradePanel.add(upgradePanelBg);

         var title = game.add.bitmapText(180, 280, 'ocra', upgrade.title, 28);
         title.align = 'center';
         _upgradePanel.add(title);

         if(doFade) {
            _upgradePanel.alpha = 0
            game.add.tween(_upgradePanel).to({ alpha: 1 }, 250, Phaser.Easing.Linear.Out, true);
         }

         function formatDescription(str) {
            var maxLineLength = 23;
            var i = 0;
            while(i < str.length) {
               i += maxLineLength;
               if(i < str.length)
                  while(str[i] != ' ') {
                     i--;
                  }
                  str = str.replaceAt(i, '\n');
            }
            return str;
         };

         var description = game.add.bitmapText(180, 320, 'ocra_small', formatDescription(upgrade.description), 18);
         description.align = 'left';
         _upgradePanel.add(description);

         var backButton = game.add.button(210, 550, 'upgrade_button', function() {
            audioButtonClick.play();
            if(!_upgradePanel.inputEnabled) return;
            closeDescription(true);
         }, null, 1, 0, 1, 0);
         _upgradePanel.add(backButton);
         _upgradePanel.add(game.add.bitmapText(210+70, 550+8, 'impact', 'BACK', 20));

         if(upgrade.level > 0 && canEquip) {
            var equipButton = game.add.button(210, 470, 'upgrade_button', function() {
               audioButtonClick.play();
               if(!_upgradePanel.inputEnabled) return;
               equipCallback(upgrade);
               closeDescription(true);
               animateText(250, 470, 'impact', 30, 0x55FF55, 'EQUIPPED');
            }, null, 1, 0, 1, 0);
            _upgradePanel.add(equipButton);
            _upgradePanel.add(game.add.bitmapText(210+70, 470+8, 'impact', 'EQUIP', 20));
         }
         
         if(upgrade.level > upgrade.cost.length-1) return;

         var upgradeButton = game.add.button(210, 510, 'upgrade_button', function() {
            if(!_upgradePanel.inputEnabled) return;
            if(upgrade.cost[upgrade.level] > Context.playerCash) {
               animateText(220, 490, 'impact', 30, 0xFF5555, 'TOO EXPENSIVE');
               return;
            }
            audioButtonClick.play();
            Context.playerCash -= upgrade.cost[upgrade.level];
            gradeCallback(upgrade, x, y);
            closeDescription();
            showDescription(upgrade, x, y, canEquip, equipCallback, gradeCallback);
            animateText(250, 510, 'impact', 30, 0x55FF55, 'UPGRADED');
         }, null, 1, 0, 1, 0);
         _upgradePanel.add(upgradeButton);
         var upgradeText = game.add.bitmapText(210+30, 510+8, 'impact', 'UPGRADE (' + upgrade.cost[upgrade.level].toString() + ')', 20);
         if(upgrade.cost[upgrade.level] > Context.playerCash)
            upgradeText.tint = 0xFFAAAA;
         else
            upgradeText.tint = 0xFFFFFF;
         _upgradePanel.add(upgradeText);
      }

      function closeDescription(doFade) {
         if(_upgradePanel.alpha < 1) return;

         function hide() {
            hp.setText(Context.playerMaxHp.toString());
            cash.setText(Context.playerCash.toString());
            _upgradePanel.removeAll(false);
            _upgradePanel.visible = false;
            _gui.inputEnabled = true;
            _buttons.inputEnabled = true;
         }

         _upgradePanel.inputEnabled = false;

         if(doFade) {
            _upgradePanel.alpha = 1
            game.add.tween(_upgradePanel).to({ alpha: 0 }, 250, Phaser.Easing.Linear.Out, true);
            setTimeout(hide, 250);
         } else {
            hide();
         }
      }

      function equipSecondaryUpgrade(upgrade) {
         var x = Context.graphics.rightSprite.x;
         var y = Context.graphics.rightSprite.y;
         Context.graphics.rightSprite.destroy();
         Context.graphics.rightSprite = game.add.sprite(x, y, upgrade.icon);
         Context.equippedUpgrade = upgrade;
         _gui.add(Context.graphics.rightSprite);
      }

      function equipBulletUpgrade(upgrade) {
         var x = Context.graphics.leftSprite.x;
         var y = Context.graphics.leftSprite.y;
         Context.graphics.leftSprite.destroy();
         Context.graphics.leftSprite = game.add.sprite(x, y, upgrade.icon);
         Context.equippedBulletUpgrade = upgrade;
         _gui.add(Context.graphics.leftSprite);
      }

      function gradeSecondaryUpgrade(upgrade, x, y) {
         upgrade.onUpgrade(upgrade.level + 1);
         var i = upgrade.level;
         var dot = game.add.sprite(x + 50 + 23*(i-1), y + 20, 'grade_dot');
         Context.gradeDots.push(dot);
         _dots.add(dot);
         equipSecondaryUpgrade(upgrade, x, y);
      }

      function gradeBulletUpgrade(upgrade, x, y) {
         upgrade.onUpgrade(upgrade.level + 1);
         var i = upgrade.level;
         var dot = game.add.sprite(x + 50 + 23*(i-1), y + 19, 'grade_dot');
         Context.gradeDots.push(dot);
         _dots.add(dot);
         equipBulletUpgrade(upgrade, x, y);
      }

      function gradeStatUpgrade(upgrade, x, y) {
         upgrade.onUpgrade(upgrade.level + 1);
         var i = upgrade.level;
         var dot = game.add.sprite(x + 50 + 23*(i-1), y + 19, 'grade_dot');
         Context.gradeDots.push(dot);
         _dots.add(dot);
      }

      /*for(var i = 0; i < 5; i++)
         addUpgrade('grade_button_1', primaryIcons[i], 137, 365+65*i);
      for(var i = 0; i < 4; i++)
         addUpgrade('grade_button_2', secondaryIcons[i], 328, 395+65*i);*/
      Context.bulletUpgrades.forEach(function(upgrade, i) {
         addUpgrade('grade_button_1', upgrade, 137, 362+67*i, true, equipBulletUpgrade, gradeBulletUpgrade);
      });

      Context.statUpgrades.forEach(function(upgrade, i) {
         addUpgrade('grade_button_1', upgrade, 137, 562+67*i, false, null, gradeStatUpgrade);
      });

      Context.secondaryUpgrades.forEach(function(upgrade, i) {
         addUpgrade('grade_button_2', upgrade, 328, 395+67*i, true, equipSecondaryUpgrade, gradeSecondaryUpgrade);
      });

      var alreadyStarted = false;

      var button_continue = game.add.button(439, 793, 'big_button', function() {
         if(_upgradePanel.visible || alreadyStarted) return;
         alreadyStarted = true;
         audioButtonClick.play();
         updateCash();
         Context.upgradeTweens.forEach(function(tween) {
            game.add.tween(tween).to({ y: -870 }, 600, Phaser.Easing.Cubic.Out, true);
         });
         function go() {
            hideUpgradeScreen(function() {
               clearPlayScreen();
               saveProgress();
               //border.destroy();
               //text.destroy();
               lastCash = Context.playerCash;
               alreadyStarted = false;
               Context.group_upgradeScreen.alpha = 1;
               startLevel(Context.levels[Context.currentLevelNumber - 1]);
            }, true);
         }
         game.add.tween(Context.group_upgradeScreen).to({ alpha: 0 }, 700, Phaser.Easing.Linear.Out, true);
         setTimeout(function() {
            animateText(300, 440, 'impact', 30, 0xFFFF00, '3...');
         }, 0);
         setTimeout(function() {
            animateText(300, 440, 'impact', 30, 0xFFFF00, '2..');
         }, 1000);
         setTimeout(function() {
            animateText(300, 440, 'impact', 30, 0xFFFF00, '1');
         }, 2000);

         setTimeout(function() {
            go();
         }, 2500);

         setTimeout(function() {
            animateText(280, 440, 'impact', 30, 0xFFFF00, 'FIGHT!');
         }, 3000);
      }, Context, 1, 0, 1, 0);
      button_continue.anchor.set(0.5);
      _buttons.add(button_continue);
      _buttons.add(game.add.bitmapText(439-27, 793-20, 'impact','NEXT', 30));

      var button_continue = game.add.button(158, 793, 'big_button', function() {
         if(_upgradePanel.visible || alreadyStarted) return;
         audioButtonClick.play();
         updateCash();
         hideUpgradeScreen(function() {
            showMenuScreen();
         });
      }, Context, 1, 0, 1, 0);
      button_continue.anchor.set(0.5);
      _buttons.add(button_continue);
      _buttons.add(game.add.bitmapText(158-30, 793-20, 'impact','MENU', 30));

      function updateCash() {
         cash.setText(Context.playerCash.toString());
      }
   },

   leftUpgradeIcon : bulletUpgrade1.icon,
   rightUpgradeIcon : noUpgrade.icon,
   
   graphics : {
      leftSprite : null,
      rightSprite : null
   },

   updateUpgradeScreen : function() {
      Context.cashText.setText(Context.playerCash.toString());
      Context.titanImage.body.angularVelocity = 10;
   },

   leftScreenBorder : null,
   rightScreenBorder : null,
   topScreenBorder : null,
   bottomScreenBorder : null,

   playerSprite : null,
   playerFireIntervalId : null,
   healthCells : null,

   updatePlayerPos : function() {
      function moveLeft() {
         //Context.playerSprite.body.velocity.x = -Context.playerSpeed;
         var speed = Context.playerSprite.body.velocity;
         var change = speed;
         var max = Context.playerSpeed;
         if(speed.x > -max - change)
            speed.x -= change;
         else
            speed.x = -max;
      };
      function moveRight() {
         var speed = Context.playerSprite.body.velocity;
         var change = speed;
         var max = Context.playerSpeed;
         //Context.playerSprite.body.velocity.x = Context.playerSpeed;
         if(speed.x < max - change)
            speed.x += change;
         else
            speed.x = max;
      };
      function stop() {
         //Context.playerSprite.body.velocity.x = 0;
         var speed = Context.playerSprite.body.velocity;
         var change = speed;
         var sprite = Context.playerSprite;
         if(sprite.x < 30) {
            sprite.x = 30;
            speed.x = 0;
            return;
         }
         if(sprite.x > 600-30) {
            sprite.x = 600-30;
            speed.x = 0;
            return;
         }
         if(speed.x > 0) {
            if(speed.x > change)
               speed.x -= change;
            else
               speed.x = 0;
         }
         if(speed.x < 0) {
            if(speed.x < change)
               speed.x += change;
            else
               speed.x = 0;
         }
      };

      if(Context.playerSprite._loose) {
         stop();
         return;
      }
      if(!game.input.pointer1) game.input.addPointer();
      if(!game.input.pointer2) game.input.addPointer();
      if(game.input.pointer1.isDown || game.input.pointer2.isDown || game.input.mousePointer.isDown) {
         if(game.input.x < 300 && this.playerSprite.x > 0+30) {
            //this.playerSprite.x -= this.playerSpeed;
            moveLeft();
         }
         else
            if(game.input.x > 300  && this.playerSprite.x < 600-30) {
               //this.playerSprite.x += this.playerSpeed;
               moveRight();
            } else {
               stop();
            }

         //******CHEAT*******************
         /*if(game.input.y < 100) {
            Context.currentLevelEnemies.forEach(function(enemy) {
               addPlayerCash(enemy.killCash);
            });
            ingame = false;
            gameWin();
         }*/
         //******************************

      } else {
         if((Akey.isDown || leftKey.isDown) && this.playerSprite.x > 0+30) {
            //this.playerSprite.x -= this.playerSpeed;
            moveLeft();
         }
         else
            if((Dkey.isDown || rightKey.isDown) && this.playerSprite.x < 600-30) {
               //this.playerSprite.x += this.playerSpeed;
               moveRight();
            } else {
               stop();
            }
      }
   },

   currentFlyingCash : 0,
   currentLevelEnemies : [],
   currentMaxEnemiesCount : null,
   currentEnemiesCount : null,
   currentPlayerBullets : [],
   currentGameIntervals : [],
   currentGameTimeouts : [],
   currentLevelNumber : 1,

   acquiredUpgrades : [healthUpgrade, attackSpeedUpgrade],
   equippedUpgrade : noUpgrade,
   equippedBulletUpgrade : bulletUpgrade1,

   upgrades : {
      doubleBullet : false,
      tripleBullet : false,
      electroCount : 5
   }
}

function preload() {
   
}

function create() {
   rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
   leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
   Akey = game.input.keyboard.addKey(Phaser.Keyboard.A);
   Dkey = game.input.keyboard.addKey(Phaser.Keyboard.D);

   Context.init();

   /*audioBackgroundMenu.play();
   audioBackgroundMenu.volume = 1;
   audioBackgroundGame.play();
   audioBackgroundGame.volume = 0;*/
   muteAudio(audioMuted);

   if(Context.showLogo) 
      showLogoScreen();
   else
      showMenuScreen();

   game.time.advancedTiming = true;
   //fps = game.add.text(0, 0, game.time.fps.toString(), { font: "24px Arial", fill: "#ffffff", align: "center" });
   buttonMute = game.add.sprite(5, 5, 'buttonMute');
   buttonMute.inputEnabled = true;
   buttonMute.events.onInputDown.add(function() {
      muteAudio(buttonMute.frame == 0);
      buttonMute.frame = buttonMute.frame == 1 ? 0 : 1;
   }, this);
   
   buttonMute.frame = audioMuted ? 1 : 0;
   audioBackgroundMenu.play();
   audioBackgroundGame.volume = 0;
}

function render() {
   
}

function update() {
   //fps.text = game.time.fps.toString();
   if(Context.group_playScreen.visible && ingame) {
      Context.updatePlayerPos();
      Context.currentLevelEnemies.forEach(function(enemy) {
         enemy.gameUpdate();
      });
      Context.currentPlayerBullets.forEach(function(bullet) {
         bullet.bulletType.update(bullet);
      });
      Context.acquiredUpgrades.forEach(function(upgrade) {
         if(upgrade.update)
            upgrade.update();
      });
      if(Context.equippedUpgrade.update)
         Context.equippedUpgrade.update();
   }
}

function showLogoScreen() {
   var background = game.add.sprite(0, 0, 'logo_background');
   var logo = game.add.sprite(game.world.centerX, game.world.centerY, 'logo');
   logo.anchor.set(0.5);
   logo.alpha = 0;

   Context.group_logoScreen.add(background);
   Context.group_logoScreen.add(logo);
   Context.group_logoScreen.visible = true;

   var animateLogo = function() {
      var animInterval = setInterval(function() {
         if(logo.alpha <= 1-0.002)
            logo.alpha += 0.002;
      }, 10);

      var animEndInterval;
      setTimeout(function() {
         clearInterval(animInterval);
         animEndInterval = setInterval(function() {
            if(logo.alpha >= 0+0.003)
               logo.alpha -= 0.003;
            if(background.alpha >= 0+0.003)
               background.alpha -= 0.003;
         }, 10);
         setTimeout(function() {
            clearInterval(animEndInterval);
            Context.group_logoScreen.visible = false;
            showMenuScreen();
         }, 3500);
      }, 2000);
   }

   animateLogo();
}

function showMenuScreen() {
   var _screen = Context.group_menuScreen;
   game.paused = false;
   _screen.forEach(function(elem) {
      elem.alpha = 1;
   });
   _screen.visible = true;
   inputMenu = true;
   var anim = setInterval(function() {
      if(_screen.alpha <= 1-0.03)
         _screen.alpha += 0.03;
   }, 10);
   setTimeout(function() {
      clearInterval(anim);
   }, 1000);
   Context.group_menuButtons.y = 270;
   Context.group_mainmenuTweens.alpha = 0;
   game.add.tween(Context.group_mainmenuTweens).to({ alpha: 1 }, 1000, Phaser.Easing.Cubic.Out, true);
   game.add.tween(Context.group_menuButtons).to({ y: 0 }, 1000, Phaser.Easing.Cubic.Out, true);
}

function showCreditScreen() {
   var _screen = Context.group_creditScreen;
   var _names = Context.group_creditNames;
   _screen.forEach(function(elem) {
      elem.alpha = 1;
   });
   _screen.visible = true;
   inputCredits = true;
   var anim = setInterval(function() {
      if(_screen.alpha <= 1-0.03)
         _screen.alpha += 0.03;
   }, 10);
   setTimeout(function() {
      clearInterval(anim);
      game.paused = false;
   }, 1000);
   _names.y = -500;
   game.add.tween(_names).to({ y: 0 }, 1000, Phaser.Easing.Cubic.Out, true);
}

function showUpgradeScreen() {
   saveProgress();
   Context.updateUpgradeScreen();
   var _screen = Context.group_upgradeScreen;
   _screen.alpha = 1;
   inputMenu = false;
   _screen.visible = true;
   game.paused = false;
   Context.upgradeTweens.forEach(function(tween) {
      tween.y = -870;
      game.add.tween(tween).to({ y: 0 }, 1000, Phaser.Easing.Cubic.Out, true);
   });

   playMenuMusic();
}

function hideMenuScreen(callback) {
   var _screen = Context.group_menuScreen;
   inputMenu = false;
   _screen.visible = false;
   callback();
}

function hideCreditScreen(callback) {
   game.add.tween(Context.group_creditNames).to({ y: -680 }, 500, Phaser.Easing.Linear.Out, true);

   setTimeout(function() {
      var _screen = Context.group_creditScreen;
      inputCredits = false;
      _screen.visible = false;
      callback();
   }, 500);
}

function hideUpgradeScreen(callback, alphaFade) {
   if(alphaFade)
      game.add.tween(Context.group_upgradeScreen).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.Out, true);

   Context.upgradeTweens.forEach(function(tween) {
      game.add.tween(tween).to({ y: -870 }, 600, Phaser.Easing.Cubic.Out, true);
   });

   setTimeout(function() {
      Context.titanImage.body.angularVelocity = 0;
      var _screen = Context.group_upgradeScreen;
      _screen.visible = false;
      Context.group_upgradeScreen.alpha = 1;
      callback();
   }, 600);
}

var firstLevelStarted = false;

function startGame() {
   if(firstLevelStarted) return;
   if(Context.currentLevelNumber == 1) {
      firstLevelStarted = true;
      game.add.tween(Context.group_menuScreen).to({ alpha: 0 }, 800, Phaser.Easing.Cubic.Out, true);
      setTimeout(function() {
         animateText(280, 440, 'impact', 30, 0x00FFFF, 'GOOD LUCK, COMMANDER'); 
      }, 500);
      setTimeout(function() {
         hideMenuScreen(function() {
            startLevel(Context.levels[Context.currentLevelNumber-1]);
            firstLevelStarted = false;
         })
      }, 800);
   } else 
      hideMenuScreen(function() {
         showUpgradeScreen();
      });
}

function startLevel(level) {
   this.init = function() {
      clearPlayScreen();

      var _units = Context.group_units = game.add.group();
      var _bullets = Context.group_bullets = game.add.group();
      var _hud = Context.group_hud = game.add.group();
      var _shield = Context.group_shield = game.add.group();

      var _screen = Context.group_playScreen;
      _screen.alpha = 0;
      _screen.inputEnabled = false;

      Context.leftScreenBorder = game.add.sprite(0,0, 'line');
      game.physics.enable(Context.leftScreenBorder, Phaser.Physics.ARCADE);
      Context.leftScreenBorder.body.immovable = true;

      Context.rightScreenBorder = game.add.sprite(600,0, 'line');
      game.physics.enable(Context.rightScreenBorder, Phaser.Physics.ARCADE);
      Context.rightScreenBorder.body.immovable = true;

      Context.topScreenBorder = game.add.sprite(0,0, 'line2');
      game.physics.enable(Context.topScreenBorder, Phaser.Physics.ARCADE);
      Context.topScreenBorder.body.immovable = true;

      Context.bottomScreenBorder = game.add.sprite(0,740, 'line2');
      game.physics.enable(Context.bottomScreenBorder, Phaser.Physics.ARCADE);
      Context.bottomScreenBorder.body.immovable = true;

      Context.acquiredUpgrades.forEach(function(upgrade) {
         upgrade.init();
      });

      hp = game.add.bitmapText(80, 870-20, 'impact',Context.playerMaxHp.toString(), 18);

      //cash = game.add.text(210, 800-29, Context.playerCash.toString(), { font: "16px Impact", fill: "#ffffff", align: "left" });
      cash = game.add.bitmapText(200, 870-29, 'impact',Context.playerCash.toString(), 20);
      //cash.setShadow(2, 2, 'rgba(0,0,0,1)', 0);

      //enemiesCount = game.add.text(310, 800-29, Context.playerCash.toString(), { font: "16px Impact", fill: "#ffffff", align: "left" });
      enemiesCount = game.add.bitmapText(310, 870-29, 'impact','', 20);
      //enemiesCount.setShadow(2, 2, 'rgba(0,0,0,0.5)', 0);

      //levelNumber = game.add.text(390, 800-29, "Level: " + Context.currentLevelNumber, { font: "16px Impact", fill: "#ffffff", align: "left" });
      levelNumber = game.add.bitmapText(390, 870-29, 'impact',"Level: " + Context.currentLevelNumber.toString(), 20);
      //levelNumber.setShadow(2, 2, 'rgba(0,0,0,0.5)', 0);

      _screen.add(Context.leftScreenBorder);
      _screen.add(Context.rightScreenBorder);
      _screen.add(Context.topScreenBorder);
      _screen.add(Context.bottomScreenBorder);
      var background = game.add.sprite(0, -20, level.background);
      _screen.add(background);

      _screen.add(_units);
      _screen.add(_bullets);
      _screen.add(_shield);
      _screen.add(_hud);

      _hud.add(game.add.sprite(-20, 870-43, 'playerBar'));
      _hud.add(game.add.sprite(170, 870-31, 'cashIcon'));
      _hud.add(game.add.sprite(275, 870-31, 'enemiesIcon'));

      var player = game.add.sprite(game.world.centerX, 870-68, 'player');
      player.anchor.setTo(0.5, 0.5);
      player.angle = -90;
      game.physics.enable(player, Phaser.Physics.ARCADE);
      player.body.immovable = true;
      player.animations.add('idle', [0], 30, true);
      player.animations.add('hurt', [1,2], 30, false);
      _units.add(player);
      player.animations.play('idle');
      Context.playerSprite = player;
      
      _screen.visible = true;

      game.add.tween(_screen).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.Out, true);

      setTimeout(function() {
         _screen.inputEnabled = true;
      }, 1000);

      var _enemies = Context.currentLevelEnemies;
      Context.currentMaxEnemiesCount = level.enemiesLayout.length;
      Context.currentEnemiesCount = level.enemiesLayout.length;
      level.enemiesLayout.forEach(function(enemy) {
         if(!enemy.params) enemy.params = {};
         if(enemy.type.preInit) {
            enemy.type.preInit(game, _units, enemy.x, enemy.y, enemy.params);
         }

         var sprite = game.add.sprite(enemy.x, enemy.y, enemy.type.sprite);
         _units.add(sprite);
         game.physics.enable(sprite, Phaser.Physics.ARCADE);
         sprite.gameUpdate = function() {
            enemy.type.update(this);
         };
         sprite.doDestroy = function() {
            if(this.shootIntervalId)
               clearInterval(this.shootIntervalId);
            clearUnitTemporas(this);
            if(enemy.type.preDestroy)
               enemy.type.preDestroy(this);
            this.destroy();
         }
         _enemies.push(sprite);
         enemy.type.init(sprite, enemy.params);
      });
      Context.currentEnemiesCount = level.enemiesLayout.length;
      level.init();

      if(Context.healthCells) {
         Context.healthCells.forEach(function(cell) {
            cell.destroy();
         });
      };
      Context.healthCells = [];
      Context.playerHp = Context.playerMaxHp;

      var startX = Context.playerHpBarStartX;
      var startY = Context.playerHpBarStartY;

      for(var i = 0; i < 12; i++) {
         Context.healthCells[i] = game.add.sprite(-i + Context.playerHpIconWidth + startX + i * Context.playerHpCellWidth, startY, 'healthCell');
         _hud.add(Context.healthCells[i]);
      };

      var hpIcon = game.add.sprite(startX-2, startY-2, 'healthIcon');
      hpIcon.anchor.setTo(0, 0.05);
      _hud.add(hpIcon);

      _hud.add(hp);
      _hud.add(cash);
      _hud.add(enemiesCount);
      _hud.add(levelNumber);
      updatePlayerCash();
      updateEnemiesCount();
      setPlayerFireInterval(Context.playerFireDelay);
      Context.equippedUpgrade.init();
      Context.equippedBulletUpgrade.init();
      game.paused = false;
   }

   this.init();
   ingame = true;
   playGameMusic();
}

function setPlayerFireInterval(interval) {
   if(Context.playerFireIntervalId)
      clearInterval(Context.playerFireIntervalId);
   Context.playerFireIntervalId = setInterval(function() {
      var eCnt = Context.upgrades.electroCount;
      var player = Context.playerSprite;
      if(Context.upgrades.doubleBullet === true) {
         playerFire({ x : player.x-10, y : player.y-35+0, electroCount: eCnt });
         playerFire({ x : player.x+10, y : player.y-35+0, electroCount: eCnt });
         return;
      }
      if(Context.upgrades.tripleBullet === true) {
         playerFire({ x : player.x+0, y : player.y-35-7, electroCount: eCnt });
         playerFire({ x : player.x-15, y : player.y-35+0, electroCount: eCnt });
         playerFire({ x : player.x+15, y : player.y-35+0, electroCount: eCnt });
         return;
      }
      playerFire({ noParams: true, electroCount: eCnt });
   }, interval);
}

function playerFire(params, bulletType) {
   if(game.paused) return;
   if(!params) {
      params = {
         noParams : true
      };
   };
   var type;
   if(bulletType)
      type = bulletType;
   else 
      type = Context.playerBulletType;
   var bullet = game.add.sprite(-500, -500, type.sprite);
   Context.group_bullets.add(bullet);
   Context.currentPlayerBullets.push(bullet);
   bullet.bulletType = type;
   bullet.ownerIsPlayer = true;
   game.physics.enable(bullet, Phaser.Physics.ARCADE);
   bullet.doDestroy = function() {
      Context.currentPlayerBullets.splice(Context.currentPlayerBullets.indexOf(this),1);
      this.destroy();
   };
   type.onFire(bullet, Context.playerSprite, params);
}

function enemyFire(enemy, bulletType, params) {
   if(game.paused) return;
   var bullet = game.add.sprite(-500, -500, bulletType.sprite);
   Context.group_bullets.add(bullet);
   Context.currentPlayerBullets.push(bullet);
   bullet.bulletType = bulletType;
   game.physics.enable(bullet, Phaser.Physics.ARCADE);
   bullet.doDestroy = function() {
      Context.currentPlayerBullets.splice(Context.currentPlayerBullets.indexOf(this),1);
      clearUnitTemporas(this);
      this.destroy();
   };
   bulletType.onFire(bullet, enemy, params);
}

function hurtPlayer(damage, deathCallback) {
   //return;
   if(Context.playerSprite._loose) return;
   audioHeroShield.play();
   Context.playerHp -= damage;

   animateText(Context.playerSprite.x, Context.playerSprite.y, 'impact', 30, 0xFF1111, (-damage).toString()); 

   Context.playerSprite.animations.play('hurt');
   addUnitTimeout(Context.playerSprite, function() {
      Context.playerSprite.animations.play('idle');
   }, 100);
   updatePlayerHealthBar();
   if(Context.playerHp <= 0) {
      Context.playerSprite.animations.play('idle');
      deathCallback();
   }
};

function hurtEnemy(enemy, damage, deathCallback) {
   enemy.currentHp -= damage;
   if(enemy.currentHp <= 0) {

      var dropCash = 250;
      if(enemy.dropCash)
      dropCash = enemy.dropCash;

      if(random.integerInRange(0, 100) <= 25 && Context.currentEnemiesCount > 1)
      enemyFire(enemy, dollarBullet, { damage : dropCash });
      enemy.killed = true;

      var killCash = 100;
      if(enemy.killCash)
      killCash = enemy.killCash;

      addPlayerCash(killCash);
      animateText(enemy.x, enemy.y, 'impact', 30, 0x11FF11, killCash.toString()+'$'); 

      Context.currentEnemiesCount--;
      updateEnemiesCount();

      audioEnemyExplode.play();

      deathCallback();

      if(Context.currentEnemiesCount <= 0) {
         setTimeout(gameWin, 2000);
      }
   } else {
      audioEnemyHurt.play();
   }
};

function updatePlayerHealthBar() {
   if(Context.playerHp < 0) Context.playerHp = 0;
   var maxCells = 12;
   var curHp = Context.playerHp;
   var step = Math.ceil(Context.playerMaxHp / maxCells);
   var active = Math.ceil(curHp / step);
   if(curHp > 0)
      Context.healthCells.forEach(function(cell, index) {
         if(index > active)
            cell.frame = 1;
         else
            cell.frame = 0;
      });
   else
      Context.healthCells.forEach(function(cell, index) {
         cell.frame = 1;
      });
   hp.setText(Context.playerHp.toString());
};

function clearPlayScreen() {
   clearGameTemporas();
   Context.currentLevelEnemies.forEach(function(elem) {
      if(elem.shootIntervalId)
         clearInterval(elem.shootIntervalId);
      elem.destroy();
      clearUnitTemporas(elem);
   });
   Context.currentPlayerBullets.forEach(function(elem) {
      elem.destroy();
   });
   Context.group_playScreen.forEach(function(elem) {
      if(elem)
         elem.destroy();
   });
   if(Context.playerSprite)
      Context.playerSprite.destroy();
   clearInterval(Context.playerFireIntervalId);
   Context.currentLevelEnemies.splice(0,Context.currentLevelEnemies.length);
   Context.currentPlayerBullets.splice(0,Context.currentPlayerBullets.length);
}

function hidePlayScreen(callback) {
   //game.paused = true;
   var _screen = Context.group_playScreen;
   game.add.tween(_screen).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.Out, true);
   setTimeout(function() {
      clearPlayScreen();
      _screen.visible = false;
      callback();
   }, 1000);
};

function setHardParams(typeObj, params) {
   if(typeof(params) != 'object') return;
   Object.keys(params).forEach(function(param) {
      typeObj[param] = params[param];
   });
};

function addGameTimeout(callback, time) {
   var t = setTimeout(callback, time);
   Context.currentGameTimeouts.push(t);
   return t;
};

function addGameInterval(callback, time) {
   var t = setInterval(callback, time);
   Context.currentGameIntervals.push(t);
   return t;
};

function clearGameTemporas() {
   Context.currentGameTimeouts.forEach(function(timeout) {
      clearTimeout(timeout);
   });
   Context.currentGameIntervals.forEach(function(interval) {
      clearInterval(interval);
   });
};

function addUnitTimeout(unit, callback, time) {
   if(!unit.unitTimeouts)
      unit.unitTimeouts = [];
   unit.unitTimeouts.push(setTimeout(callback, time));
};

function addUnitInterval(unit, callback, time) {
   if(!unit.unitIntervals)
      unit.unitIntervals = [];
   unit.unitIntervals.push(setInterval(callback, time));
};

function clearUnitTemporas(unit) {
   if(unit.unitTimeouts) {
      unit.unitTimeouts.forEach(function(timeout) {
         clearTimeout(timeout);
      });
   };
   if(unit.unitIntervals) {
      unit.unitIntervals.forEach(function(interval) {
         clearTimeout(interval);
      });
   };
};

function addPlayerCash(amount) {
   Context.playerCash += amount;
   updatePlayerCash();
};

function updatePlayerCash() {
   cash.text = Context.playerCash.toString();
   cash.visible = true;
};

function updateEnemiesCount() {
   enemiesCount.setText(Context.currentEnemiesCount.toString() + "/" + Context.currentMaxEnemiesCount.toString());
};

function gameWin() {
   if(Context.playerSprite._loose) return;
   Context.currentLevelNumber++;
   if(Context.currentLevelNumber > Context.levels.length)
      Context.currentLevelNumber = 1;
   hidePlayScreen(function() {
      ingame = false;
      showUpgradeScreen();
   });
};

function gameLoose() {
   if(Context.playerSprite._loose) return;
   Context.playerSprite._loose = true;
   clearUnitTemporas(Context.playerSprite);
   clearGameTemporas();
   clearInterval(Context.playerFireIntervalId);
   var blink = setInterval(function() {
      Context.playerSprite.visible =! Context.playerSprite.visible;
   }, 250);
   setTimeout(function() {
      clearInterval(blink);
      Context.playerSprite.visible = true;
      Context.playerSprite._loose = false;
      Context.playerCash = lastCash;
      hidePlayScreen(function() {
         if(Context.currentLevelNumber > 1)
            showUpgradeScreen();
         else {
            playMenuMusic();
            showMenuScreen();
         }
      });
      ingame = false;
   }, 3500);
};

function acquireUpgrade(upgrade) {
   Context.acquiredUpgrades.push(upgrade);
};

function muteAudio(_mute) {
   localStorage.setItem('audioMuted', _mute ? 'yes' : 'no');
   audioMuted = _mute ? true : false;
   audios.forEach(function(audio) {
      audio.volume = _mute ? 0 : 0.5;
   });

   audioEnemyShotOrange.volume = _mute ? 0 : 0.3;
   audioEnemyShotGreen.volume = _mute ? 0 : 0.3;

   audioHeroShield.volume = _mute ? 0 : 1.5;
   audioHeroLaserShot.volume = _mute ? 0 : 1.5;
   audioHeroMissle.volume = _mute ? 0 : 0.9;
   audioButtonClick.volume = _mute ? 0 : 1.5;
   audioEnemyExplode.volume = _mute ? 0 : 5;

   if(!_mute) {
      audioBackgroundGame.volume = ingame ? 1 : 0;
      audioBackgroundMenu.volume = ingame ? 0 : 1;
   }
};

function saveProgress() {
   localStorage.setItem(SAVE_PREFIX+'currentLevel', Number(Context.currentLevelNumber));
   localStorage.setItem(SAVE_PREFIX+'cash', Number(Context.playerCash));
   Context.allUpgrades.forEach(function(upgrade, index) {
      localStorage.setItem(SAVE_PREFIX+'lvl-'+index, Number(upgrade.level));
   });
   localStorage.setItem(SAVE_PREFIX+'equippedUpgrade', Number(Context.allUpgrades.indexOf(Context.equippedUpgrade)));
   localStorage.setItem(SAVE_PREFIX+'equippedBulletUpgrade', Number(Context.allUpgrades.indexOf(Context.equippedBulletUpgrade)));
}

function restoreProgress() {
   Context.currentLevelNumber = Number(localStorage.getItem(SAVE_PREFIX+'currentLevel')) || 1;
   Context.playerCash = Number(localStorage.getItem(SAVE_PREFIX+'cash')) || 0;
   lastCash = Context.playerCash;
   Context.allUpgrades.forEach(function(upgrade, index) {
      upgrade.level = Number(localStorage.getItem(SAVE_PREFIX+'lvl-'+index.toString()) || 0);
   });
   if(bulletUpgrade1.level < 1)
      bulletUpgrade1.level = 1;
   if(localStorage.getItem(SAVE_PREFIX+'equippedUpgrade'))
      Context.equippedUpgrade = Context.allUpgrades[Number(localStorage.getItem(SAVE_PREFIX+'equippedUpgrade'))];
   if(localStorage.getItem(SAVE_PREFIX+'equippedBulletUpgrade'))
      Context.equippedBulletUpgrade = Context.allUpgrades[Number(localStorage.getItem(SAVE_PREFIX+'equippedBulletUpgrade'))];
}

function resetProgress() {
   localStorage.removeItem(SAVE_PREFIX+'currentLevel');
   localStorage.removeItem(SAVE_PREFIX+'cash');
   Context.allUpgrades.forEach(function(upgrade, index) {
      upgrade.level = 0;
      localStorage.removeItem(SAVE_PREFIX+'lvl-'+index);
   });
   localStorage.removeItem(SAVE_PREFIX+'equippedUpgrade');
   localStorage.removeItem(SAVE_PREFIX+'equippedBulletUpgrade');

   Context.currentLevelNumber = 1;
   Context.playerCash = 0;

   location.reload(true);
}

function playMenuMusic() {
   if(audioBackgroundGame.volume > 0) {
      game.add.tween(audioBackgroundGame).to({ volume: 0 }, 1000, Phaser.Easing.Linear.Out, true);
   }
   setTimeout(function() {
         audioBackgroundGame.stop();
   }, 1000);
   if(!audioMuted && audioBackgroundMenu.volume == 0) {
      audioBackgroundMenu.volume = 0;
      audioBackgroundMenu.play();
      game.add.tween(audioBackgroundMenu).to({ volume: 1 }, 650, Phaser.Easing.Linear.Out, true);
   }
}

function playGameMusic() {
   if(firstLaunch) {
      audioBackgroundGame.play();
      firstLaunch = false;
   }
   if(audioBackgroundMenu.volume > 0) {
      var tw = game.add.tween(audioBackgroundMenu);
      tw.to({ volume: 0 }, 650, Phaser.Easing.Linear.Out, true);
      tw.onComplete.add(function() {
         audioBackgroundMenu.stop();
      }, this);
   }
   if(!audioMuted && audioBackgroundGame.volume == 0) {
      audioBackgroundGame.volume = 0;
      audioBackgroundGame.play();
      game.add.tween(audioBackgroundGame).to({ volume: 1 }, 1000, Phaser.Easing.Linear.Out, true);
   }
}

function animateText(x, y, font, size, color, text) {
   setTimeout(function() {
      var _text = game.add.bitmapText(x, y, font, text.toString(), size);
      _text.tint = color;
      game.add.tween(_text).to({ y: _text.y-50, alpha: 0 }, 1000, Phaser.Easing.Linear.Out, true);

      setTimeout(function() {
         _text.destroy();
      }, 1000);
   },0);
}