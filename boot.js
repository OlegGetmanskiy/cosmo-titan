/*************************
*    Cosmo Titan         *
* Boot js file           *
* Author: Oleg Getmansky *
*************************/

window.onload = function() {

};

var Game = {};

Game.Boot = function(game) {

};

Game.Boot.prototype = {
   preload : function() {
      this.load.image('preloadBar', ASSETS_LOAD_PREFIX + 'bootload.png');
      this.load.image('preloadBg', ASSETS_LOAD_PREFIX + 'preload_bg.png');
   },
   create : function() {
      this.stage.disableVisibilityChange = true;
      this.scale.minWidth = 300;
      this.scale.minHeight = 400;
      this.scale.maxWidth = 2400;
      this.scale.maxHeight = 3200;
      this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      this.scale.pageAlignHorizontally = true;
      this.scale.pageAlignVeritcally = true;
      
      if(!this.game.device.desktop) {
         this.input.maxpointers = 2;
      }
      this.scale.setScreenSize(true);
      this.state.start('Preloader');
   }
};