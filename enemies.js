var enemySoldier = {
   sprite : 'enemies/soldier.png',
   speed : 75,
   shootDelay : 1500,
   maxHp : 19,
   currentHp : null,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 55, 55);
   },
   init : function(_this, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.angle = 90;
      _this.animations.add('idle', [0], 30);
      _this.animations.add('shoot', [1,2,3,4,5,6], 30);
      _this.body.immovable = true;

      _this.moveDirection = params.moveDirection;
      _this.body.velocity.x = this.speed * _this.moveDirection;

      _this.currentHp = this.maxHp;
      
      setHardParams(_this, params);

      setTimeout(function() {
         _this.shootIntervalId = setInterval(function() {
            _this.animations.play('shoot');
            addGameTimeout(function() {
               enemyFire(_this, bullet1);
            }, 333); 
         }, 1500);
      }, random.integerInRange(250, 1000));
   },
   update : function(_this) {
      game.physics.arcade.overlap(_this, Context.leftScreenBorder, function() {
         _this.body.velocity.x = this.speed;
      }, null, this);
      game.physics.arcade.overlap(_this, Context.rightScreenBorder, function() {
         _this.body.velocity.x = -this.speed;
      }, null, this);
   }
};

var enemyFighter = {
sprite : 'enemies/fighter.png',
maxHp : 30,
currentHp : null,
speed : 100,
topBorder : 100,
botBorder : 250,
preload : function(game) {
game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 45, 58);
},
init : function(_this, params) {
_this.anchor.setTo(0.5, 0.5);
_this.angle = 90;
_this.animations.add('idle', [0], 1);
_this.animations.add('shoot', [1,2,3,4,5,6], 20);
_this.body.immovable = true;
_this.currentHp = this.maxHp;

_this.xMinBorder = 0+30;
_this.xMaxBorder = 600-30;
_this.topBorder = enemyFighter.topBorder;
_this.botBorder = enemyFighter.botBorder;
_this.speed = enemyFighter.speed;

setHardParams(_this, params);

_this.targetX = random.integerInRange(_this.xMinBorder, _this.xMaxBorder);
_this.targetY = random.integerInRange(_this.topBorder, _this.botBorder);

setTimeout(function() {
_this.shootIntervalId = setInterval(function() {
_this.animations.play('shoot');
enemyFire(_this, bullet2);
}, 1500);
}, random.integerInRange(250, 1000));

enemyFighter.setMoveToTarget(_this);
/*_this.randomMoveId = setInterval(function() {
enemyFighter.randomizeMove(_this);
}, 1000);*/
},
update : function(_this) {
this.checkIfNearTarget(_this, function() {
//_this.body.velocity.setTo(0,0);
_this.targetX = random.integerInRange(_this.xMinBorder, _this.xMaxBorder);
_this.targetY = random.integerInRange(_this.topBorder, _this.botBorder);
enemyFighter.setMoveToTarget(_this);
});
},
setMoveToTarget : function(_this) {
var dX = _this.targetX - _this.x;
var dY = _this.targetY - _this.y;
var angle = Math.atan2(dY , dX);
var speedX = Math.cos(angle) * _this.speed;
var speedY = Math.sin(angle) * _this.speed;

_this.body.velocity.x = speedX;
_this.body.velocity.y = speedY;
},
checkIfNearTarget : function(_this, callback) {
if(Math.abs(_this.x - _this.targetX) <= _this.speed/10 && Math.abs(_this.y - _this.targetY) <= _this.speed/10) {
callback();
};
}
};

var enemyArmadillo = {
   sprite : 'enemies/armadillo.png',
   speed : 25,
   maxHp : 40,
   currentHp : null,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 46, 40);
   },
   init : function(_this, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.angle = 90;
      _this.body.immovable = true;
      _this.moveDirection = params.moveDirection;
      _this.body.velocity.x = this.speed * _this.moveDirection;

      setHardParams(_this, params);

      _this.currentHp = this.maxHp;
      _this.animations.add('idle');
      _this.animations.play('idle', 30, true);
   },
   update : enemySoldier.update
};

var enemyHeavyArmadillo = {
   sprite : 'enemies/heavyArmadillo.png',
   maxHp : 170,
   currentHp : null,
   speed : 15,
   targetX : 500,
   targetY : 400,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 50, 54);
   },
   init : function(_this, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.angle = 90;
      _this.body.immovable = true;
      _this.body.velocity.y = this.speed;
      _this.speed = enemyHeavyArmadillo.speed;
      _this.targetX = enemyHeavyArmadillo.targetX;
      _this.targetY = enemyHeavyArmadillo.targetY;

      setHardParams(_this, params);

      _this.currentHp = this.maxHp;

      _this.animations.add('idle', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 30, true);
      _this.animations.add('shoot', [16,17,18,19,20], 30);

      setTimeout(function() {
         _this.shootIntervalId = setInterval(function() {
            _this.animations.play('shoot');
            addGameTimeout(function() {
               enemyFire(_this, bullet2, { noParams: true });
            }, 150); 
            setTimeout(function(){
               _this.animations.play('idle', true);
            },150);
         }, 2000);
      }, random.integerInRange(250, 1000));

      _this.animations.play('idle', true);
      this.setMoveToTarget(_this);
   },
   update : function(_this) {
      this.checkIfNearTarget(_this, function() {
         _this.body.velocity.setTo(0,0);
      });
   },
   setMoveToTarget : function(_this) {
      var dX = _this.targetX - _this.x;
      var dY = _this.targetY - _this.y;
      var angle = Math.atan2(dY , dX);
      var speedX = Math.cos(angle) * _this.speed;
      var speedY = Math.sin(angle) * _this.speed;

      _this.body.velocity.x = speedX;
      _this.body.velocity.y = speedY;
   },
   checkIfNearTarget : function(_this, callback) {
      if(Math.abs(_this.x - _this.targetX) <= _this.speed/10 && Math.abs(_this.y - _this.targetY) <= _this.speed/10) {
         callback();
      };
   }
};

//Горизонтальная турель
var enemyTurret1 = {
   sprite : 'enemies/turret_1.png',
   maxHp : 18,
   speed : 8,
   startX : 0,
   shootCount : 3,
   attackDelay : 600,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 48, 44);
   },
   init : function(_this, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.angle = 90;
      _this.body.immovable = true;
      _this.speed = enemyHeavyArmadillo.speed;
      _this.targetX = enemyHeavyArmadillo.targetX;
      _this.targetY = enemyHeavyArmadillo.targetY;
      _this.shootCount = enemyTurret1.shootCount;

      setHardParams(_this, params);

      _this.currentHp = this.maxHp;
      _this.shootRemain = _this.shootCount;

      _this.animations.add('idle', [0], 30, true);
      _this.animations.add('shoot', [1,2,3,4,5,6,7,8,9,10,11,12,13], 30);

      enemyTurret1.goMove(_this);
   },
   update : function(_this) {
      if(!_this.moving) return;

      if(Math.abs(_this.x - _this.targetX) <= enemyTurret1.speed) {
         _this.x = _this.targetX;
         enemyTurret1.goFire(_this);
         return;
      }

      if(_this.x < _this.targetX) 
         _this.x += enemyTurret1.speed;
      else
         if(_this.x > _this.targetX) 
            _this.x -= enemyTurret1.speed;
   },
   goFire : function(_this) {
      _this.moving = false;
      _this.shootRemain = _this.shootCount;
      for(var i = 0; i < _this.shootCount; i++) {
         addUnitTimeout(_this, function() {
            enemyTurret1.shoot(_this);
         }, enemyTurret1.attackDelay * i);
      }
      addUnitTimeout(_this, function() {
         enemyTurret1.goMove(_this);
      }, enemyTurret1.attackDelay * (_this.shootCount + 1));
   },
   goMove : function(_this) {
      _this.moving = true;
      _this.targetX = random.integerInRange(0, 600);
      _this.shootRemain = 0;
   },
   shoot : function(_this) {
      _this.animations.play('shoot');
      addUnitTimeout(_this, function() {
         enemyFire(_this, bullet1, { site: 'left', x: -10 });
         enemyFire(_this, bullet1, { site: 'right', x: +10 });
      }, 150); 
      setTimeout(function(){
         _this.animations.play('idle', true);
      },150+300);
   },
   preDestroy : function(_this) {
      _this.moving = false;
      clearUnitTemporas(_this);
   }
};

//Вертикальная турель
var enemyTurret2 = {
   base : 'enemies/platformTurret_2.png',
   sprite : 'enemies/turret_2.png',
   maxHp : 60,
   attackDelay : 3200,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 61, 38);
      game.load.image(this.base, ASSETS_LOAD_PREFIX + this.base);
   },
   preInit : function(game, screen, x, y, params) {
      //_this.turretBaseSprite = game.add.image(x, y, this.base);
      var baseSprite = game.add.image(x, y, this.base);
      baseSprite.anchor.setTo(0.5, 0.5);
      screen.add(baseSprite);
      params.unit = { };
      params.unit.baseSprite = baseSprite;
   },
   init : function(_this, params) {
      _this.baseTurretSprite = params.unit.baseSprite;

      _this.anchor.setTo(0.25, 0.5);
      _this.angle = 90;
      _this.body.immovable = true;
      _this.body.velocity.x = this.speed;
      _this.speed = enemyHeavyArmadillo.speed;
      _this.targetX = enemyHeavyArmadillo.targetX;
      _this.targetY = enemyHeavyArmadillo.targetY;
      _this.shootCount = enemyTurret1.shootCount;

      setHardParams(_this, params);

      _this.currentHp = this.maxHp;
      _this.shootRemain = _this.shootCount;

      _this.animations.add('idle', [0], 30, true);
      _this.animations.add('shoot', [1,2,3,4,5,6,7,8], 30);

      _this.shootIntervalId = setInterval(function() {
         enemyTurret2.shoot(_this);
      }, 1000);
   },
   update : function(_this) {
      _this.rotation = game.physics.arcade.angleBetween(_this, Context.playerSprite);
   },
   shoot : function(_this) {
      _this.animations.play('shoot');
      addGameTimeout(function() {
         enemyFire(_this, bullet2, { angle: _this.angle, visibleTimeout: 25 });
      }, 150); 
      setTimeout(function(){
         _this.animations.play('idle', true);
      },150);
   },
   preDestroy : function(_this) {
      clearUnitTemporas(_this);
      _this.baseTurretSprite.destroy();
   }
};

//Кондиционер
var enemyNeutralizer = {
   sprite : 'enemies/neutralizer.png',
   speed : 5,
   shootDelay : 3000,
   maxHp : 55,
   currentHp : null,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 51, 74);
   },
   init : function(_this, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.angle = 90;
      _this.animations.add('idle', [0], 30);
      _this.animations.add('shoot', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,
         24,25,26,27,28,29,30,31,32], 30);
      _this.body.immovable = true;

      _this.moveDirection = params.moveDirection;
      _this.body.velocity.x = this.speed * _this.moveDirection;

      _this.currentHp = this.maxHp;
      
      setHardParams(_this, params);

      addUnitTimeout(_this, function() {
         _this.shootIntervalId = setInterval(function() {
            _this.animations.play('shoot');
            addUnitTimeout(_this, function() {
               enemyFire(_this, neutralizerBullet);
            }, 1000); 
         }, enemyNeutralizer.shootDelay);
      }, random.integerInRange(0, 500));
   },
   update : function(_this) {
      game.physics.arcade.overlap(_this, Context.leftScreenBorder, function() {
         _this.body.velocity.x = this.speed;
      }, null, this);
      game.physics.arcade.overlap(_this, Context.rightScreenBorder, function() {
         _this.body.velocity.x = -this.speed;
      }, null, this);
   }
};