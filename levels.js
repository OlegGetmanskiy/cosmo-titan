var level1 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemySoldier, x: 300, y: 310, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 420, y: 270, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 180, y: 270, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}

var level2 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyArmadillo, x: 50, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 100, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 550, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 450, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 500, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 150, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 200, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 400, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 80, y: 410, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 180, y: 410, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 130, y: 380, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 520, y: 410, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 420, y: 410, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 470, y: 380, params: {moveDirection:  -1, killCash: 100} }
   ],
   init : function() {
      
   }
}

var level3 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [

{ type: enemyArmadillo, x: 550, y: 530, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 490, y: 530, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 520, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 430, y: 530, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 460, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 580, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 20, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 80, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 140, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 50, y: 530, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 110, y: 530, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 170, y: 530, params: {moveDirection: 1, killCash: 100} },
{ type: enemyFighter, x: 300, y: 280, params: {xMinBorder: 20, xMaxBorder:  580, topBorder:  260, botBorder:  360, killCash: 600} },
{ type: enemySoldier, x: 230, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 370, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 300, y: 210, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 380, y: 190, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 210, y: 190, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 300, y: 410, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}
var level4 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyHeavyArmadillo, x: 150, y: 250, params: {targetX:  110, targetY:  650, killCash: 800} },
{ type: enemyHeavyArmadillo, x: 450, y: 250, params: {targetX:  490, targetY:  650, killCash: 800} },
{ type: enemyArmadillo, x: 90, y: 460, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 150, y: 460, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 190, y: 550, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 250, y: 550, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 510, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 450, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 350, y: 550, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 410, y: 550, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 220, y: 510, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 380, y: 510, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 120, y: 510, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 480, y: 510, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 40, y: 40, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 100, y: 70, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 160, y: 40, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 220, y: 70, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 160, y: 110, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 380, y: 70, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 500, y: 70, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 440, y: 120, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 440, y: 40, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 560, y: 40, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}
var level5 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyHeavyArmadillo, x: 110, y: 240, params: {targetX:  150, targetY:  650, killCash: 800} },
{ type: enemyHeavyArmadillo, x: 490, y: 240, params: {targetX:  450, targetY:  650, killCash: 800} },
{ type: enemyArmadillo, x: 140, y: 410, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 110, y: 370, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 170, y: 370, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 170, y: 450, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 110, y: 450, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 460, y: 410, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 430, y: 450, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 490, y: 450, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 430, y: 370, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 490, y: 370, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyFighter, x: 200, y: 160, params: {xMinBorder: 20, xMaxBorder:  580, topBorder:  130, botBorder:  220, killCash: 600} },
{ type: enemyFighter, x: 420, y: 160, params: {xMinBorder: 20, xMaxBorder:  580, topBorder:  130, botBorder:  220, killCash: 600} },
{ type: enemySoldier, x: 300, y: 50, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 360, y: 50, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 330, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 240, y: 50, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 270, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 190, y: 90, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 410, y: 90, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}

var level6 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemySoldier, x: 300, y: 170, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 300, y: 230, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 350, y: 200, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 250, y: 200, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 250, y: 260, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 350, y: 260, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 300, y: 290, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 480, y: 390, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 480, y: 450, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 530, y: 420, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 430, y: 420, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 120, y: 450, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 170, y: 420, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 70, y: 420, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 120, y: 380, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}

var level7 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyTurret1, x: 90, y: 10, params: {killCash:  650} },
{ type: enemyTurret1, x: 520, y: 10, params: {killCash:  650} },
{ type: enemyTurret1, x: 270, y: 10, params: {killCash:  650} },
{ type: enemyTurret1, x: 430, y: 10, params: {killCash:  650} },
{ type: enemyTurret1, x: 340, y: 10, params: {killCash:  650} },
{ type: enemyTurret1, x: 180, y: 10, params: {killCash:  650} },
{ type: enemyArmadillo, x: 300, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 350, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 400, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 450, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 500, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 550, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 250, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 200, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 150, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 100, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 50, y: 580, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 180, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 80, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 130, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 230, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 280, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 330, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 380, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 430, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 480, y: 540, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 530, y: 540, params: {moveDirection:  -1, killCash: 100} }
   ],
   init : function() {
      
   }
}

var level8 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyTurret1, x: 80, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 530, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 190, y: 10, params: {killCash: 650} },
{ type: enemyArmadillo, x: 40, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 100, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 160, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 220, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 280, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 340, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 400, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 460, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 520, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 580, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemyFighter, x: 60, y: 160, params: {xMinBorder: 20, xMaxBorder:  580, topBorder:  130, botBorder:  260, killCash: 600} },
{ type: enemyFighter, x: 490, y: 220, params: {xMinBorder: 20, xMaxBorder:  580, topBorder:  130, botBorder:  260, killCash: 600} },
{ type: enemyArmadillo, x: 140, y: 290, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 200, y: 290, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 170, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 400, y: 290, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 460, y: 290, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 520, y: 290, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 430, y: 330, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 490, y: 330, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 80, y: 290, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 110, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 150, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 150, y: 550, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 210, y: 520, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 90, y: 520, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 390, y: 520, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 450, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 510, y: 520, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 450, y: 550, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}

var level9 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyArmadillo, x: 140, y: 410, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 190, y: 410, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 230, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 380, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 470, y: 400, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 420, y: 400, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 30, y: 550, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 570, y: 550, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 520, y: 550, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 80, y: 550, params: {moveDirection: 1, killCash: 100} },
{ type: enemyTurret1, x: 110, y: 10, params: {killCash: 650} },
{ type: enemySoldier, x: 80, y: 160, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 200, y: 160, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 400, y: 160, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 520, y: 160, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyTurret1, x: 300, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 520, y: 10, params: {killCash: 650} },
{ type: enemyHeavyArmadillo, x: 100, y: 320, params: {targetX:  100, targetY:  650, killCash: 800} },
{ type: enemyHeavyArmadillo, x: 300, y: 320, params: {targetX:  250, targetY:  650, killCash: 800} },
{ type: enemyHeavyArmadillo, x: 500, y: 320, params: {targetX:  500, targetY:  650, killCash: 800} }
   ],
   init : function() {
      
   }
}

var level10 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyArmadillo, x: 30, y: 510, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 80, y: 510, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 130, y: 510, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 570, y: 510, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 520, y: 510, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 470, y: 510, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 190, y: 240, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 250, y: 240, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 310, y: 240, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 370, y: 240, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 430, y: 240, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 220, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 280, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 340, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 400, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 280, y: 190, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 340, y: 190, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 310, y: 140, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 310, y: 350, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 180, y: 420, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 230, y: 430, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 370, y: 430, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 420, y: 420, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyTurret2, x: 570, y: 220, params: {killCash: 700} },
{ type: enemyTurret2, x: 30, y: 220, params: {killCash: 700} }
   ],
   init : function() {
      
   }
}

var level11 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyNeutralizer, x: 150, y: 340, params: {moveDirection: -1, killCash: 650} },
{ type: enemyNeutralizer, x: 450, y: 340, params: {moveDirection: 1, killCash: 650} },
{ type: enemyTurret2, x: 50, y: 210, params: {killCash: 700} },
{ type: enemyTurret2, x: 550, y: 210, params: {killCash: 700} },
{ type: enemyTurret1, x: 80, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 550, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 230, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 420, y: 10, params: {killCash: 650} },
{ type: enemyArmadillo, x: 310, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 260, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 210, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 360, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 280, y: 550, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 230, y: 550, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 330, y: 550, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}

var level12 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemySoldier, x: 140, y: 50, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 100, y: 80, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 180, y: 80, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 140, y: 110, params: {moveDirection: 1, killCash: 100} },
{ type: enemyNeutralizer, x: 180, y: 210, params: {moveDirection:  -1, killCash: 650} },
{ type: enemyNeutralizer, x: 90, y: 210, params: {moveDirection:  -1, killCash: 650} },
{ type: enemySoldier, x: 420, y: 80, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 460, y: 50, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 500, y: 80, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 460, y: 120, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyNeutralizer, x: 420, y: 210, params: {moveDirection: 1, killCash: 650} },
{ type: enemyNeutralizer, x: 510, y: 210, params: {moveDirection: 1, killCash: 650} },
{ type: enemyFighter, x: 140, y: 320, params: {xMinBorder: 20, xMaxBorder:  580, topBorder: 230, botBorder: 450, killCash: 600} },
{ type: enemyFighter, x: 300, y: 390, params: {xMinBorder: 20, xMaxBorder:  580, topBorder: 230, botBorder: 450, killCash: 600} },
{ type: enemyFighter, x: 450, y: 310, params: {xMinBorder: 20, xMaxBorder:  580, topBorder: 230, botBorder: 460, killCash: 600} },
{ type: enemyArmadillo, x: 80, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 130, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 180, y: 490, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 520, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 470, y: 490, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 420, y: 490, params: {moveDirection:  -1, killCash:  100} },
{ type: enemyArmadillo, x: 450, y: 530, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 500, y: 530, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 160, y: 530, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 110, y: 530, params: {moveDirection: 1, killCash: 100} },
{ type: enemyNeutralizer, x: 260, y: 210, params: {moveDirection:  -1, killCash: 650} },
{ type: enemyNeutralizer, x: 340, y: 210, params: {moveDirection: 1, killCash: 650} },
{ type: enemySoldier, x: 190, y: 130, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 220, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 400, y: 130, params: {moveDirection:  -1, killCash: 100} },
{ type: enemySoldier, x: 370, y: 100, params: {moveDirection:  -1, killCash: 100} }
   ],
   init : function() {
      
   }
}
var level13 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyHeavyArmadillo, x: 90, y: 90, params: {targetX:  120, targetY:  420, killCash: 800} },
{ type: enemyHeavyArmadillo, x: 510, y: 90, params: {targetX:  480, targetY:  420, killCash: 800} },
{ type: enemyArmadillo, x: 210, y: 570, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 260, y: 570, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 390, y: 570, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 340, y: 570, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 370, y: 610, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 230, y: 610, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 420, y: 220, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 480, y: 220, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 520, y: 280, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 480, y: 330, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 460, y: 280, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 120, y: 330, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 140, y: 280, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 80, y: 280, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 120, y: 220, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 180, y: 220, params: {moveDirection: 1, killCash: 100} },
{ type: enemyNeutralizer, x: 520, y: 490, params: {moveDirection:  -1, killCash: 650} },
{ type: enemyNeutralizer, x: 440, y: 490, params: {moveDirection:  -1, killCash: 650} },
{ type: enemyNeutralizer, x: 160, y: 490, params: {moveDirection: 1, killCash: 650} },
{ type: enemyNeutralizer, x: 80, y: 490, params: {moveDirection: 1, killCash: 650} },
{ type: enemyArmadillo, x: 410, y: 630, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 430, y: 590, params: {moveDirection:  -1, killCash: 100} },
{ type: enemyArmadillo, x: 190, y: 630, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 170, y: 590, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}
var level14 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyTurret2, x: 300, y: 200, params: {killCash: 700} },
{ type: enemySoldier, x: 370, y: 280, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 300, y: 280, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 230, y: 280, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 260, y: 330, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 330, y: 330, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 400, y: 330, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 190, y: 330, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 230, y: 380, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 300, y: 380, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 370, y: 380, params: {moveDirection: 1, killCash: 100} },
{ type: enemyTurret1, x: 70, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 530, y: 10, params: {killCash: 650} },
{ type: enemyArmadillo, x: 20, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 70, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 120, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 170, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 220, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 270, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 320, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 370, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 420, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 470, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 520, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 570, y: 460, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 110, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 160, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 130, y: 140, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 520, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 460, y: 100, params: {moveDirection: 1, killCash: 100} },
{ type: enemySoldier, x: 490, y: 140, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 550, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 500, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 450, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 400, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 350, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 300, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 250, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 200, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 150, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 100, y: 500, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 50, y: 500, params: {moveDirection: 1, killCash: 100} }
   ],
   init : function() {
      
   }
}
var level15 = {
   background : 'menu/background_MainMenu.png',
   enemiesLayout : [
{ type: enemyHeavyArmadillo, x: 90, y: 410, params: {targetX:  90, targetY:  650, killCash: 800} },
{ type: enemyHeavyArmadillo, x: 220, y: 410, params: {targetX:  220, targetY:  650, killCash: 800} },
{ type: enemyHeavyArmadillo, x: 510, y: 410, params: {targetX:  510, targetY:  650, killCash: 800} },
{ type: enemyHeavyArmadillo, x: 380, y: 410, params: {targetX:  380, targetY:  650, killCash: 800} },
{ type: enemyTurret1, x: 50, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 530, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 170, y: 10, params: {killCash: 650} },
{ type: enemyTurret1, x: 420, y: 10, params: {killCash: 650} },
{ type: enemyArmadillo, x: 20, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 70, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 120, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 170, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 220, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 270, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 320, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 370, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 420, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 470, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 520, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 570, y: 340, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 550, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 500, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 450, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 400, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 350, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 300, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 250, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 200, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 150, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 100, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyArmadillo, x: 50, y: 300, params: {moveDirection: 1, killCash: 100} },
{ type: enemyFighter, x: 150, y: 160, params: {xMinBorder: 20, xMaxBorder:  580, topBorder:  50, botBorder:  250, killCash: 600} },
{ type: enemyFighter, x: 450, y: 170, params: {xMinBorder: 20, xMaxBorder:  580, topBorder:  50, botBorder:  250, killCash: 600} },
{ type: enemyTurret2, x: 310, y: 160, params: {killCash: 700} },
{ type: enemyNeutralizer, x: 150, y: 480, params: {moveDirection: 1, killCash: 650} },
{ type: enemyNeutralizer, x: 440, y: 480, params: {moveDirection:  -1, killCash: 650} }
   ],
   init : function() {
      
   }
}