/*************************
*    Cosmo Titan         *
* Preloader file         *
* Author: Oleg Getmansky *
*************************/

Game.Preloader = function(game) {
   this.preloadBar = null;
   this.ready = false;
};

Game.Preloader.prototype = {
   preload : function() {
      var prx = ASSETS_LOAD_PREFIX;

      this.preloadBar = this.add.sprite(this.world.centerX, this.world.centerY, 'preloadBar');
      this.add.sprite(97, 383, 'preloadBg');
      this.preloadBar.anchor.setTo(0.5, 0.5);
      this.load.setPreloadSprite(this.preloadBar);

      this.load.spritesheet('buttonMute', prx + 'menu/button_sound.png', 51, 51);
      this.load.image('logo_background', prx + 'logo_background.png');
      this.load.image('logo', prx + 'logo.png');
      this.load.image('playerBar', prx + 'playerBar.png');

      this.load.image('credits_bg', prx + 'credits_bg.png');

      this.load.image('grade_bg', prx + 'grade_bg.png');
      this.load.image('menu_titan', prx + 'menu/titan.png');
      this.load.spritesheet('big_button', prx + 'menu/big_button.png', 105, 65);
      this.load.spritesheet('grade_button_1', prx + 'menu/grade_button_1.png', 135, 59);
      this.load.spritesheet('grade_button_2', prx + 'menu/grade_button_2.png', 102, 59);
      this.load.spritesheet('upgrade_button', prx + 'menu/upgrade_button.png', 179, 42);
      this.load.image('upgrade_panel_bg', prx + 'menu/upgrade_panel_bg.png');
      this.load.image('grade_dot', prx + 'menu/grade_dot.png');

      this.load.spritesheet('healthCell', prx + 'healthCell.png', 9, 18);
      this.load.image('healthIcon', prx + 'healthIcon.png');
      this.load.image('cashIcon', prx + 'cashIcon.png');
      this.load.image('enemiesIcon', prx + 'enemiesIcon.png');
      this.load.image('line', prx + 'line.png');
      this.load.image('line2', prx + 'line2.png');
      this.load.image('background_border', prx + 'menu/background_border.png');
      this.load.image('background_MainMenu', prx + 'menu/background_MainMenu.png');
      this.load.image('mainmenu_e1', prx + 'menu/element_1.png');
      this.load.image('mainmenu_e2', prx + 'menu/element_2.png');
      this.load.spritesheet('button_MainMenu', prx + 'menu/button_MainMenu.png', 328, 52);
      this.load.spritesheet('player', prx + 'player.png', 49, 66);
      this.load.spritesheet('explosionSmall_1', prx + 'effects/explosionSmall_1.png', 83, 80, 17);
      this.load.spritesheet('heroHit1', prx + 'effects/heroHit1.png', 21, 20, 6);

      game.load.bitmapFont('impact', prx + 'font/impact_0.png', prx + 'font/impact.fnt');
      game.load.bitmapFont('ocra', prx + 'font/ocra_0.png', prx + 'font/ocra.fnt');
      game.load.bitmapFont('ocra_small', prx + 'font/ocra_small_0.png', prx + 'font/ocra_small.fnt');

      //Ключами для спрайтов левелов, врагов и пуль
      //являются сами пути к файлам
      Context.levels.forEach(function(level) {
         this.load.image(level.background, prx + level.background, false);
      }, this);
      Context.enemies.forEach(function(enemy) {
         enemy.preload(this);
      }, this);
      Context.allUpgrades.forEach(function(upgrade) {
         upgrade.preload(this);
         this.load.image(upgrade.icon, prx + upgrade.icon, false);
      }, this);
      Context.bullets.forEach(function(bullet) {
         if(bullet.preload)
            bullet.preload(this)
         else
            this.load.image(bullet.sprite, prx + bullet.sprite, false);
      }, this);

      game.load.audio('background_game', prx + 'sound/background_game.mp3');
      game.load.audio('background_menu', prx + 'sound/background_menu.mp3');
      game.load.audio('button_click', prx + 'sound/button_click.wav');
      game.load.audio('hero_shot', prx + 'sound/hero_shot.wav');
      game.load.audio('hero_missle', prx + 'sound/hero_missle.wav');
      game.load.audio('hero_shield', prx + 'sound/hero_shield.wav');
      game.load.audio('enemy_shot_orange', prx + 'sound/enemy_shot_orange.wav');
      game.load.audio('enemy_shot_green', prx + 'sound/enemy_shot_green.wav');
      game.load.audio('enemy_explode', prx + 'sound/enemy_explode.wav');
      game.load.audio('enemy_hurt', prx + 'sound/enemy_hurt.wav');
      game.load.audio('hero_laser_shot', prx + 'sound/hero_laser_shot.wav');
   },
   create : function() {
      this.preloadBar.cropEnabled = false;
   },
   update : function() {
      if (this.cache.isSoundDecoded('background_menu') && this.ready == false)
      {
         this.ready = true;
         this.state.start('Ingame');
      }
   }
};