/*************************
*    Cosmo Titan         *
* Upgrades definitions   *
* Author: Oleg Getmansky *
*************************/

laserUpgrade = {
   sprite : 'upgrades/laser.png',
   icon : 'menu/icons/laser.png',
   title : 'Laser',
   description : 'You sometimes shoot the laser what deals instant mass damage in vertical direction',
   cost : [8000, 5000],
   level : 0,
   damage : 5,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 51, 885);
   },
   init : function() {
      addGameInterval(this.fire, 4000 - 1500 * this.level);
   },
   fire : function() {
      if(!Context.group_playScreen.visible) return;
      var _screen = Context.group_bullets;
      var laserSprite = game.add.sprite(Context.playerSprite.x, Context.playerSprite.y, laserUpgrade.sprite);
      _screen.add(laserSprite);
      laserSprite.anchor.setTo(0.5, 1);
      laserSprite.animations.add('fire', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 30);
      laserSprite.animations.play('fire');

      audioHeroLaserShot.play();
      Context.currentLevelEnemies.forEach(function(enemy) {
         if(enemy.killed) return;
         if(Math.abs(laserSprite.x-enemy.x) < enemy.width/2+10) {
            hurtEnemy(enemy, laserUpgrade.damage, function() {
               bullet1.explode(laserSprite, enemy);
               enemy.doDestroy();
            });
         }
      });

      laserUpgrade._laser = laserSprite;

      addGameTimeout(function() {
         laserSprite.destroy();
      }, 500);
   },
   update : function() {
      if(laserUpgrade._laser) {
         laserUpgrade._laser.x = Context.playerSprite.x;
      }
   },
   onUpgrade : function(newLevel) {
      if(newLevel > 2) return;
      this.level = newLevel;
   }
}

var homingMissleUpgrade = {
   title : 'Homing missle',
   icon : 'menu/icons/rocket.png',
   description : 'You sometimes shoot missle what homes enemies',
   cost : [4000, 3000],
   level : 0,
   preload : function(game) {

   },
   init : function() {
      addGameInterval(function() {
         audioHeroMissle.play();
         playerFire({ x: Context.playerSprite.x - 30, y: Context.playerSprite.y, angle: 0 }, homingMissle);
         playerFire({ x: Context.playerSprite.x + 30, y: Context.playerSprite.y, angle: 180 }, homingMissle);
      }, 2000 - 500 * this.level);
   },
   onUpgrade : function(newLevel) {
      if(newLevel > 2) return;
      this.level = newLevel;
   }
}

var immortalShield = {
   sprite : 'upgrades/shield.png',
   icon : 'menu/icons/shield.png',
   title : 'Invulnerability',
   description : 'You can get energy shield absorbing all the damage',
   cost : [5000, 4000],
   level : 0,
   duration : 1500,
   interval : 8000,
   shieldSprite : null,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 103, 103);
   },
   init : function() {
      this.shieldSprite = game.add.sprite(Context.playerSprite.x+5, Context.playerSprite.y, this.sprite);
      var shield = this.shieldSprite;
      game.physics.enable(shield, Phaser.Physics.ARCADE);
      shield.body.angularVelocity = -2;
      Context.group_shield.add(shield);
      shield.alpha = 0;
      shield.anchor.setTo(0.5, 0.5);
      shield.animations.add('idle', [0], 30);
      shield.animations.add('hit', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24], 30, true);
      shield.animations.play('hit', true);
      this.duration = 2000 + 1000 * (this.level - 1); //1 = 2000, 2 = 3000
      addGameTimeout(this.show, 3000);
   },
   update : function() {
      var _this = this.shieldSprite;
      if(_this.x < 30) {
         _this.x = 30;
         _this.body.velocity.x = 0;
         return;
      }
      if(_this.x > 600-30) {
         _this.x = 600-30;
         _this.body.velocity.x = 0;
         return;
      }
      this.shieldSprite.body.velocity.x = Context.playerSprite.body.velocity.x;
      if(this.shieldSprite.alpha <= 0) return;
      game.physics.arcade.overlap(this.shieldSprite, Context.currentPlayerBullets, function(_this, bullet) {
         if(!bullet.ownerIsPlayer && bullet.bulletType != dollarBullet && this.distance(_this, bullet) <= 95) {
            bullet.doDestroy();
         }
      }, null, this);
   },
   setShield : function() {
      if(!Context.group_playScreen.visible) return;
      var shield = immortalShield.shieldSprite;
      shield.x = Context.playerSprite.x+5;
      shield.alpha = 0;
      //audioHeroShield.play();
      game.add.tween(shield).to({ alpha: 1.1 }, 700, Phaser.Easing.Linear.Out, true);
   },
   unsetShield : function() {
      if(!Context.group_playScreen.visible) return;
      var shield = immortalShield.shieldSprite;
      shield.alpha = 1.1;
      game.add.tween(shield).to({ alpha: 0 }, 700, Phaser.Easing.Linear.Out, true);
      addGameTimeout(immortalShield.show, immortalShield.interval);
   },
   show : function() {
      immortalShield.setShield();
      addGameTimeout(immortalShield.unsetShield, immortalShield.duration);
   },
   distance : function(point1, point2) {
      var xs = 0;
      var ys = 0;
      xs = point2.x - point1.x;
      xs = xs * xs;
      ys = point2.y - point1.y;
      ys = ys * ys;
      return Math.sqrt( xs + ys );
   },
   onUpgrade : function(newLevel) {
      if(newLevel > 2) return;
      this.level = newLevel;
   }
}

var extraBullets = {
   title : 'Extra Bullets',
   icon : 'menu/icons/double.png',
   description : 'Sometimes you shoot two extra bullets with regular attack',
   cost : [2000, 1500],
   level : 0,
   interval : 2000,
   preload : function() {

   },
   init : function() {
      addGameInterval(function() {
         audioHeroShot.play();
         playerFire({ x : Context.playerSprite.x-25, y : Context.playerSprite.y-35-7, electroCount: 0 }, heroBullet1);
         playerFire({ x : Context.playerSprite.x+25, y : Context.playerSprite.y-35-7, electroCount: 0 }, heroBullet1);
      }, extraBullets.interval - 500*extraBullets.level);
   },
   onUpgrade : function(newLevel) {
      if(newLevel > 2) return;
      this.level = newLevel;
   }
}

var noUpgrade = {
   title : 'No upgrade',
   icon : 'menu/icons/none.png',
   cost : [0, 0],
   description : '',
   level : 0,
   preload : function() {

   },
   init : function() {

   },
   update : function() {

   },
   onUpgrade : function(newLevel) {
      if(newLevel > 2) return;
      this.level = newLevel;
   }
}

var bulletUpgrade1 = {
   title : 'Regular bullets',
   icon : 'menu/icons/bullet_1.png',
   description : 'Just a regular bullets. One, two or three at a time as you upgrade',
   cost : [300, 500, 2500],
   level : 1,
   preload : function() {

   },
   init : function() {
      Context.playerBulletType = heroBullet1;
      Context.upgrades.doubleBullet = this.level == 2;
      Context.upgrades.tripleBullet = this.level == 3;
   },
   update : function() {

   },
   onUpgrade : function(newLevel) {
      if(newLevel > 3) return;
      this.level = newLevel;
   }
}

var bulletUpgrade2 = {
   title : 'Ghost Bullets',
   description : 'Ghost bullets can go through enemies, dealing damage in vertical direction',
   icon : 'menu/icons/bullet_2.png',
   cost : [6500, 3600, 6000],
   level : 0,
   preload : function() {

   },
   init : function() {
      Context.playerBulletType = heroBullet2;
      Context.upgrades.electroCount = this.level;
      Context.upgrades.doubleBullet = this.level == 2;
      Context.upgrades.tripleBullet = this.level == 3;
   },
   update : function() {

   },
   onUpgrade : function(newLevel) {
      if(newLevel > 3) return;
      this.level = newLevel;
   }
}

var bulletUpgrade3 = {
   title : 'Ricochet bullets',
   description : 'When ricochet bullet hits an enemy, two extra bullets is shot from it, dealing damage in horizontal direction',
   icon : 'menu/icons/bullet_3.png',
   cost : [7000, 4000, 6000],
   level : 0,
   preload : function() {

   },
   init : function() {
      Context.playerBulletType = heroBullet3;
      Context.upgrades.electroCount = this.level;
      Context.upgrades.doubleBullet = this.level == 2;
      Context.upgrades.tripleBullet = this.level == 3;
   },
   update : function() {

   },
   onUpgrade : function(newLevel) {
      if(newLevel > 3) return;
      this.level = newLevel;
   }
}

var healthUpgrade = {
   title : 'Health Upgrade',
   description : 'Your titan\'s toughness. Lose it all and you\'re dead',
   icon : 'menu/icons/hp.png',
   cost : [1500, 5000, 7000],
   level : 0,
   preload : function() {

   },
   init : function() {
      Context.playerMaxHp = 20 + this.level * 10;
      Context.playerHp = Context.playerMaxHp;
   },
   update : function() {

   },
   onUpgrade : function(newLevel) {
      if(newLevel > 3) return;
      this.level = newLevel;
      this.init();
   }
}

var attackSpeedUpgrade = {
   title : 'Attack Speed',
   description : 'The more, the faster you shoot',
   icon : 'menu/icons/speed.png',
   cost : [6000, 6000, 6000],
   level : 0,
   preload : function() {

   },
   init : function() {
      Context.playerFireDelay = 500 - 25 * this.level;
   },
   update : function() {

   },
   onUpgrade : function(newLevel) {
      if(newLevel > 3) return;
      this.level = newLevel;
   }
}