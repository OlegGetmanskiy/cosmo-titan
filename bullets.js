/*************************
*    Cosmo Titan         *
* Bullets descriptions   *
* Author: Oleg Getmansky *
*************************/

//Оранжевая
var bullet1 = {
   sprite : 'bullets/bullet_enemy_1.png',
   damage : 5,
   speed : 400,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 23, 8);
   },
   onFire : function(_this, actor, params) {
      _this.x = actor.x;
      _this.anchor.setTo(0.5, 0.5);
      if(actor === Context.playerSprite) {
         _this.y = actor.y-30;
         _this.angle = -90;
         _this.body.velocity.y = -bullet1.speed;
      } else {
         _this.y = actor.y+30;
         _this.angle = 90;
         _this.body.velocity.y = bullet1.speed;
      }

      if(params) {
         _this.x += params.x;
      }
      _this.animations.add('regular', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 30, true);
      _this.animations.play('regular', true);
      audioEnemyShotOrange.play();
   },
   update : function(_this) {
      if(_this.y < 0 || _this.y > 790) {
         _this.doDestroy();
      }
      /*game.physics.arcade.collide(_this, [Context.topScreenBorder, Context.bottomScreenBorder], function() {
         _this.doDestroy();
      }, null, this);*/
      if(_this.angle === -90) {
         game.physics.arcade.overlap(_this, Context.currentLevelEnemies, function(bullet, enemy) {
            bullet1.explode(_this, enemy);
            hurtEnemy(enemy, this.damage, function() {
               enemy.doDestroy();
               this.explode(_this, enemy);
            });
            _this.doDestroy();
         }, null, this);
      }
      game.physics.arcade.overlap(_this, Context.playerSprite, function() {
         bullet1.explode(_this, Context.playerSprite);
         _this.doDestroy();
         hurtPlayer(this.damage, gameLoose);
      }, null, this);
   },
   explode : function(_this, victim) {
      var anim = game.add.sprite(victim.x, victim.y, 'explosionSmall_1', 0);
      Context.group_playScreen.add(anim);
      anim.anchor.setTo(0.5, 0.5);
      anim.animations.add('explode', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 30, false);
      anim.animations.play('explode');
      setTimeout(function() {
         anim.destroy();
      },500);
   }
}

//Зеленая
var bullet2 = {
   sprite : 'bullets/bullet_enemy_2.png',
   damage : 7,
   speed : 600,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 32, 12);
   },
   onFire : function(_this, actor, params) {
      _this.x = actor.x;
      _this.anchor.setTo(0.5, 0.5);
      if(actor === Context.playerSprite) {
         _this.y = actor.y-30;
         _this.angle = -90;
         _this.body.velocity.y = -this.speed;
      } else {
         _this.y = actor.y;
         _this.angle = 90;
         _this.body.velocity.y = this.speed;
      }
      if(params && !params.noParams) {
         _this.angle = params.angle;
         _this.x = actor.x;
         _this.y = actor.y;
         _this.body.velocity.x = Math.cos(_this.angle / 180 * Math.PI) * this.speed;
         _this.body.velocity.y = Math.sin(_this.angle / 180 * Math.PI) * this.speed;
         if(params.visibleTimeout) {
            _this.visible = false;
            addUnitTimeout(_this, function() {
               _this.visible = true;
            }, params.visibleTimeout);
         }
      }
      _this.animations.add('regular', [0,1,2,3,4,5,6,7,8,9,10,11], 30, true);
      _this.animations.play('regular', true);
      audioEnemyShotGreen.play();
   },
   update : function(_this) {
      if(_this.y < 0 || _this.y > 790) {
         _this.doDestroy();
      }
      /*game.physics.arcade.collide(_this, [Context.topScreenBorder, Context.bottomScreenBorder], function() {
         _this.doDestroy();
      }, null, this);*/
      if(_this.angle === -90) {
         game.physics.arcade.overlap(_this, Context.currentLevelEnemies, function(bullet, enemy) {
            this.explode(_this, enemy);
            hurtEnemy(enemy, this.damage, function() {
               enemy.doDestroy();
               this.explode(_this, enemy);
            });
            _this.doDestroy();
         }, null, this);
      }
      game.physics.arcade.overlap(_this, Context.playerSprite, function() {
         this.explode(_this, Context.playerSprite);
         _this.doDestroy();
         hurtPlayer(this.damage, gameLoose);
      }, null, this);
   },
   explode : bullet1.explode
}

var heroBullet1 = {
   sprite : 'bullets/bullet_hero_1.png',
   damage : 4,
   speed : 800,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 23, 8);
   },
   onFire : function(_this, actor, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.angle = -90;
      _this.body.velocity.y = -this.speed;

      if(params.noParams) {
         _this.x = actor.x;
         _this.y = actor.y-35;
      } else {
         _this.x = params.x;
         _this.y = params.y;
      }
      _this.electroCount = params.electroCount;
      if(params.noDamageEnemy)
         _this.noDamageEnemy = params.noDamageEnemy;
      else
         _this.noDamageEnemy = null;
      _this.animations.add('regular', [0,1,2,3,4,5,6], 30, true);
      _this.animations.play('regular', true);
      audioHeroShot.play();
   },
   update : function(_this) {
      if(_this.y < 0 || _this.y > 870) {
         _this.doDestroy();
      }
      /*game.physics.arcade.collide(_this, [Context.topScreenBorder, Context.bottomScreenBorder], function() {
         _this.doDestroy();
      }, null, this);*/
      game.physics.arcade.overlap(_this, Context.currentLevelEnemies, function(bullet, enemy) {
         if(_this.noDamageEnemy !== enemy) {
            this.explode(_this, enemy);
            hurtEnemy(enemy, this.damage, function() {
               bullet1.explode(_this, enemy);
               enemy.doDestroy();
            })
            _this.doDestroy();
         }
      }, null, this);
   },
   explode : function(_this, victim) {
      var anim = game.add.sprite(_this.x, _this.y, 'heroHit1', 0);
      Context.group_playScreen.add(anim);
      anim.anchor.setTo(0.5, 1);
      anim.animations.add('explode', [0,1,2,3,4,5], 30, false);
      anim.animations.play('explode');
      setTimeout(function() {
         anim.destroy();
      },200);
   }
}

var heroBullet2 = {
   sprite : 'bullets/bullet_hero_2.png',
   damage : 4,
   speed : 800,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 36, 16);
   },
   onFire : function(_this, actor, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.angle = -90;
      _this.body.velocity.y = -this.speed;

      if(params.noParams) {
         _this.x = actor.x;
         _this.y = actor.y-35;
      } else {
         _this.x = params.x;
         _this.y = params.y;
      }
      _this.electroCount = params.electroCount;
      if(params.noDamageEnemy)
         _this.noDamageEnemy = params.noDamageEnemy;
      else
         _this.noDamageEnemy = null;
      _this.animations.add('regular', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 30, true);
      _this.animations.play('regular', true);
      audioHeroShot.play();
   },
   update : function(_this) {
      if(_this.y < 0 || _this.y > 790) {
         _this.doDestroy();
      }
      /*game.physics.arcade.collide(_this, [Context.topScreenBorder, Context.bottomScreenBorder], function() {
         _this.doDestroy();
      }, null, this);*/
      game.physics.arcade.overlap(_this, Context.currentLevelEnemies, function(bullet, enemy) {
         if(_this.noDamageEnemy !== enemy) {
            this.explode(_this, enemy);
            if(_this.electroCount > 0) {
               playerFire({ x: _this.x, y: _this.y, electroCount: 0, noDamageEnemy: enemy });
            }
            hurtEnemy(enemy, this.damage, function() {
               bullet1.explode(_this, enemy);
               enemy.doDestroy();
            });
            _this.doDestroy();
         }
      }, null, this);
   },
   explode : function(_this, victim) {
      heroBullet1.explode(_this, victim);
   }
}

var heroBullet3 = {
   sprite : 'bullets/bullet_hero_3.png',
   damage : 3,
   speed : 800,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 16, 16);
   },
   onFire : function(_this, actor, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.angle = -90;
      if(!params.hor)
         _this.body.velocity.y = -this.speed;
      else
         _this.body.velocity.x = this.speed * params.multi;

      if(params.noParams) {
         _this.x = actor.x;
         _this.y = actor.y-35;
      } else {
         _this.x = params.x;
         _this.y = params.y;
      }
      _this.electroCount = params.electroCount;
      if(params.noDamageEnemy)
         _this.noDamageEnemy = params.noDamageEnemy;
      else
         _this.noDamageEnemy = null;
      _this.animations.add('regular', [0,1,2,3,4,5,6,7,8,9,10], 30, true);
      _this.animations.play('regular', true);
      audioHeroShot.play();
   },
   update : function(_this) {
      if(_this.y < 0 || _this.y > 790 || _this.x < 0 || _this.x > 600) {
         _this.doDestroy();
      }
      game.physics.arcade.overlap(_this, Context.currentLevelEnemies, function(bullet, enemy) {
         if(_this.noDamageEnemy !== enemy) {
            this.explode(_this, enemy);
            if(_this.electroCount > 0) {
               playerFire({ hor: true, multi: 1, x: _this.x, y: _this.y-35, electroCount: 0, noDamageEnemy: enemy, multiplier: -1 });
               playerFire({ hor: true, multi: -1, x: _this.x, y: _this.y-35, electroCount: 0, noDamageEnemy: enemy, multiplier: 1 });
            }
            hurtEnemy(enemy, this.damage, function() {
               bullet1.explode(_this, enemy);
               enemy.doDestroy();
            });
            _this.doDestroy();
         }
      }, null, this);
   },
   explode : function(_this, victim) {
      heroBullet1.explode(_this, victim);
   }
}

// =)
var dollarBullet = {
   sprite : 'dollar.png',
   damage : 1,
   speed : 300,
   preload : function(game) {
      game.load.image(this.sprite, ASSETS_LOAD_PREFIX + this.sprite);
   },
   onFire : function(_this, actor, params) {
      _this.x = actor.x;
      _this.anchor.setTo(0.5, 0.5);
      _this.y = actor.y+30;
      _this.angle = 90;
      _this.body.gravity.y = 500;
      _this.body.velocity.y = -200;
      _this.body.velocity.x = random.integerInRange(50, 100);
      _this.body.angularVelocity = random.integerInRange(350, 700);
      if(random.integerInRange(0,1) === 1) {
         _this.body.velocity.x = -_this.body.velocity.x;
         _this.body.angularVelocity = -_this.body.angularVelocity;
      }
      _this.damage = params.damage; 
      Context.currentFlyingCash++;
   },
   update : function(_this) {
      if(_this.y < 0 || _this.y > 790) {
         _this.doDestroy();
         Context.currentFlyingCash--;
         /*if(Context.currentEnemiesCount === 0 && Context.currentFlyingCash <= 0)
            gameWin();*/
      }
      game.physics.arcade.collide(_this, [Context.leftScreenBorder, Context.rightScreenBorder], function() {
         
      }, null, this);
      game.physics.arcade.overlap(_this, Context.playerSprite, function() {
         _this.doDestroy();
         Context.currentFlyingCash--;
         addPlayerCash(_this.damage);
         audioButtonClick.play();
         animateText(Context.playerSprite.x, Context.playerSprite.y, 'impact', 30, 0x11FF11, _this.damage.toString()+'$'); 
         /*if(Context.currentEnemiesCount == 0 && Context.currentFlyingCash <= 0)
            gameWin();*/
      }, null, this);
   },
   explode : function(_this, victim) {
      var anim = game.add.sprite(_this.x, _this.y, 'heroHit1', 0);
      Context.group_playScreen.add(anim);
      anim.anchor.setTo(0.5, 1);
      anim.animations.add('explode', [0,1,2,3,4,5], 30, false);
      anim.animations.play('explode');
      setTimeout(function() {
         anim.destroy();
      },200);
   }
}

var neutralizerBullet = {
   sprite : 'bullets/bullet_enemy_3.png',
   damage : 1,
   speed : 350,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 37, 47);
   },
   onFire : function(_this, actor, params) {
      _this.x = actor.x;
      _this.anchor.setTo(0.5, 0.5);
      if(actor === Context.playerSprite) {
         _this.y = actor.y-30;
         _this.angle = -90;
         _this.body.velocity.y = -this.speed;
      } else {
         _this.y = actor.y+30;
         _this.angle = 90;
         _this.body.velocity.y = this.speed;
      }

      if(params) {
         _this.x += params.x;
      }
      _this.animations.add('regular', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 30, true);
      _this.animations.play('regular', true);
   },
   update : function(_this) {
      if(_this.y < 0 || _this.y > 790) {
         _this.doDestroy();
      }
      /*game.physics.arcade.collide(_this, [Context.topScreenBorder, Context.bottomScreenBorder], function() {
         _this.doDestroy();
      }, null, this);*/
      game.physics.arcade.overlap(_this, Context.currentPlayerBullets, function(_this, bullet) {
         if(bullet.ownerIsPlayer)
            bullet.doDestroy();
      }, null, this);

      game.physics.arcade.overlap(_this, Context.playerSprite, function() {
         _this.doDestroy();
         hurtPlayer(this.damage, gameLoose);
      }, null, this);
   },
   explode : function(_this, victim) {
      var anim = game.add.sprite(victim.x, victim.y, 'explosionSmall_1', 0);
      Context.group_playScreen.add(anim);
      anim.anchor.setTo(0.5, 0.5);
      anim.animations.add('explode', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 30, false);
      anim.animations.play('explode');
      setTimeout(function() {
         anim.destroy();
      },500);
   }
}

var homingMissle = {
   sprite : 'bullets/rocket.png',
   damage : 7,
   speed : 400,
   preload : function(game) {
      game.load.spritesheet(this.sprite, ASSETS_LOAD_PREFIX + this.sprite, 35, 12);
   },
   onFire : function(_this, actor, params) {
      _this.anchor.setTo(0.5, 0.5);
      _this.body.velocity.y = -this.speed;
      setHardParams(_this, params);

      var liveEnemies = Context.currentLevelEnemies.filter(function(enemy) {
         return !enemy.killed;
      });

      if(liveEnemies.length > 0)
         _this.homeTarget = liveEnemies[random.integerInRange(0, liveEnemies.length-1)];
      else
         _this.homeTarget = null;

      _this.animations.add('regular', [0,1,2,3,4,5,6,7], 30, true);
      _this.animations.play('regular', true);
      _this.body.velocity.x = Math.cos(_this.rotation) * this.speed;
      _this.body.velocity.y = Math.sin(_this.rotation) * this.speed;
      _this._frameCounter = 0;
   },
   lerp_dir : function(cur_dir, tar_dir, inc) {  
      if (Math.abs( tar_dir - cur_dir) <= inc || Math.abs( tar_dir - cur_dir) >= (360 - inc)) {
         cur_dir = tar_dir;
      } else {
         if (Math.abs( tar_dir - cur_dir) > 180) {
            if (tar_dir < cur_dir) {
               tar_dir += 360;
            } else {
               tar_dir -= 360;
            }
         }
         if ( tar_dir > cur_dir) {
            cur_dir += inc;
         } else {
            if ( tar_dir < cur_dir) {
               cur_dir -= inc;
            }
         }
      }
      return cur_dir;
   },
   update : function(_this) {
      heroBullet1.update(_this);

      _this._frameCounter++;
      if(_this._frameCounter == 5) {
         _this._frameCounter = 0;
         if(_this.homeTarget)
         if(!_this.homeTarget.killed) {
            var targetAngle = game.physics.arcade.angleBetween(_this, _this.homeTarget);
            _this.rotation = homingMissle.lerp_dir(_this.rotation, targetAngle, 0.30);
         }
         _this.body.velocity.x = Math.cos(_this.rotation) * this.speed;
         _this.body.velocity.y = Math.sin(_this.rotation) * this.speed;
      }
   },
   explode : heroBullet1.explode
}